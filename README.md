# Graph-Com - Proyek Akhir

Di proyek akhir ini, kita membuat sebuah scene animasi interaktif 
berbasis WebGL.

## How To Run/Compile Main Apps
1. First of all, disable strict_origin by opening mozilla firefox, go to "about:config", and change the "strict_origin" to false. 
2. Open ./index.html
3. Click "Demo/Interactive Mode" button to show interactivity user interface (UI) (Interactive Mode)
4. Play with slider, dropdown, and button to interact with aquarium and character. 
5. Click "Demo/Interactive Mode" again to reload page and hide interactivity UI (Demo Mode).

## How To Run/Compile shape-demo

1. To open the Shape-Demo: Open each HTML file ./shape-demo to demonstrate each unique shape and animation. 
2. You can switch between each HTML file but selecting the appropriate dropdown menu on each HTML file.
3. Play with slider to interact with each shape.

## Members (and their Work Logs)

* Andika Hanavian Atmam (1606918585)
    - Lighting and Shading (Providing branch called shade-and-lighting, etc.)
    - Create and providing animation button to others

* Jovanta Anugerah Pelawi (1606875863)
    - Create dance move "Moonwalk" to be applied to Spongebob, Patrick and Squidward
    - Implement move coordinate of objects for "Moonwalk" to WebGL

* Muzakki Hassan Azmi (1606917632)
	- Setup object shaping procedure (later refactored by Troy)
	- Setup hierarchical object building procedure (later refactored by Troy)
	- Setup animation procedure
	- Spongebob Shaping
	- Animate Spongebob (Squiliam Dance)
	- Squidward Shaping
	- Animate Squidward (Squiliam Dance)
	- Lighting and Shading
	- Lighting interactivity
	- Shadow implementation (has not succeeded, implemented, code block commented due to bug)

* Troy Amadeus (1506730275):
	- Patrick Shaping (Adapted from Spongebob and Squidward's shaping).
	- Animate Patrick (Reusing Spongebob and Squidward's character animation).
	- Gary Shaping
	- Animate Gary (GARY_COLLIDE_NEWTON and GARY_LINEAR_NEWTON)
	- Jellyfish Shaping
	- Animate Jellyfish (JELLYFISH_SWIMMING)
	- Aquarium Shaping (Texturing based on gfxfundamentals implementation and Blending Cube with CULL_FACE)
	- Generate textures/edit-aquarium-texture-map/aquarium-texture-map.png using GIMP.
	- "Demo/Interactive Mode" Interactivity
	- "Main Camera Rotation X-Axis, Y-Axis, Z-Axis, and Zoom" Interactivity	
	- "Draw Mode" Interactivity (Draw with gl.TRIANGLES or gl.LINES)
	- "Dance Move" Interactivity (Choose Spongebob, Squidward, and Patrick dance move)
	- "Gary's Shake" Interactivity (Choose how Gary shake his eyes)
	- "Number of Jellyfish" Interactivity
	- "Play/Stop Animation" Interactivity
	- Code documentation of above implementation.
	- ./shape-demo to demonstrate each shape.
	- "Camera Mode" Interactivity.
	- Point lighting (has not succeeded)
	
## External Resources For Implementation

UV Mapping Cube Texture Mapping Three-dimensional Space Dice PNG* by hairstyleshighlights: 
	https://imgbin.com/png/HUiKTBqB/uv-mapping-cube-texture-mapping-three-dimensional-space-dice-png

Updated WebGL Library by Greg Tavares on gfxfundamentals's (webglfundamentals.org) repository:
	https://github.com/gfxfundamentals/webgl-fundamentals/tree/master/webgl/resources

WebGL Textures Tutorial by gfxfundamentals (webglfundamentals.org):
	https://webglfundamentals.org/webgl/lessons/webgl-3d-textures.html
	
Implementing texture atlas on cube by gfxfundamentals (webglfundamentals.org):
	https://codepen.io/pen/?&editable=true&editors=101=https%3A%2F%2Fwebglfundamentals.org%2Fwebgl%2Flessons%2Fwebgl-3d-textures.html

## External Resources for Texturing

GIMP:
	https://www.gimp.org/

Edit GIF Image:
	https://ezgif.com/

Convert Image to PNG:
	https://image.online-convert.com/convert-to-png

bubbletown-002.png from Spongebob Squarepants FANDOM (spongebob.fandom.com):
	https://vignette.wikia.nocookie.net/spongebob/images/2/26/Bubbletown_002.png/revision/latest?cb=20181028171458

frosted-glass-texture.png from textures4photoshop.com:
	http://www.textures4photoshop.com/tex/glass/frosted-glass-texture.aspx
	
spongebob-flower-blue-yellow-pink.png by Kimberley Louise Dear:
	https://kimdgraphics.wordpress.com/2020/01/28/unit-seven-animation/

spongebob-flower-dark-blue.png by Tobias P.:
	https://picsart.com/i/sticker-spongebob-flower-blue-303177339170211

spongebob-flower-green.png by Tobias P.:
	https://picsart.com/i/sticker-spongebob-flower-green-grun-blume-304201312350211

spongebob-flower-light-blue.png by Tobias P.:
	https://picsart.com/i/sticker-spongebob-flower-blue-303153856190211

spongebob-flower-violet.png by Tobias P.:
	https://picsart.com/en_in/i/sticker-spongebob-flower-violet-lila-water-ananas-pineapple-303177448172211

spongebob-sand.png extracted from GIF image by @spongebob:
	https://giphy.com/gifs/spongebob-spongebob-squarepants-season-6-3oKHWh2PSp6mxGZG9i

sqpantsup.png:
	https://gamebanana.com/textures/download/3843
