"use strict";

import { GARY_COLLIDE_NEWTON, GARY_LINEAR_NEWTON } from "./js/animation-frame.js";
import { SSP_MOONWALK, SSP_SQUILLIAM_DANCE, JELLYFISH_SWIMMING } from "./js/animation-frame.js";
import { colors, points, normals, drawGary } from "./js/gary-shaping.js";
import { drawJellyfish } from "./js/jellyfish-shaping.js";
import { drawPatrick } from "./js/patrick-shaping.js";
import { drawSpongebob } from "./js/spongebob-shaping.js";
import { drawSquidward } from "./js/squidward-shaping.js";
import { degToRad, setGeometry, setNormals, setTexture } from "./js/aquarium-shaping.js";

var pointLightPosition = vec4(0.0, 0.0, 0.0, 0.0);
var ambientProduct = vec4(0.3, 0.3, 0.3, 1.0);
var diffuseProduct = vec4(1.0, 1.0, 1.0, 1.0);
var specularProduct = vec4(1.0, 1.0, 1.0, 1.0);
var shininess = 15;

var dirLightIntensity = vec4(0.5, 0.5, 0.5, 1.0);
var dirLightDirection = vec4(1.0, 1.0, 1.0, 0.0);

export var gl, shadowProgramBg, shadowProgramFloor, shadowBgMatrixLoc, shadowFloorMatrixLoc, characterProgram;
var aquariumProgram;

window.onload = function init(){
	
	document.getElementsByClassName("interactive")[0].style= "display: none;";
	document.getElementsByClassName("interactive")[1].style= "display: none;";
	document.getElementsByClassName("interactive")[2].style= "display: none;";
	
	// Get A WebGL context
	/** @type {HTMLCanvasElement} */
    var canvas = document.getElementById( "gl-canvas" );
	gl = canvas.getContext("webgl");
    if (!gl){ 
		alert("WebGL isn't available");
	}
	
	main(gl)	
}

function main(gl){
	
	gl.clearColor( 1.0, 1.0, 1.0, 1.0 );
	
    //  Setup GLSL aquariumProgram by loading shaders and initializing attribute buffers
	characterProgram = webglUtils.createProgramFromScripts(gl, ["vertex-shader", "fragment-shader"]);
    aquariumProgram = webglUtils.createProgramFromScripts(gl, ["vertex-shader-3d", "fragment-shader-3d"]);
    //shadowProgramBg = webglUtils.createProgramFromScripts(gl, ["vertex-shadow-bg", "fragment-shadow"]);
    //shadowProgramFloor = webglUtils.createProgramFromScripts(gl, ["vertex-shadow-floor", "fragment-shadow"]);
	
	// look up where the vertex data needs to go.
	var vPosition = gl.getAttribLocation( characterProgram, "vPosition" );
	var vColor = gl.getAttribLocation( characterProgram, "vColor" );
	var vNormal = gl.getAttribLocation( characterProgram, "vNormal" );
	var positionLocation = gl.getAttribLocation(aquariumProgram, "a_position");
	var texcoordLocation = gl.getAttribLocation(aquariumProgram, "a_texcoord");
	var vNormal2 = gl.getAttribLocation(aquariumProgram, "vNormal");

	// lookup uniforms
	var modelViewMatrixLoc = gl.getUniformLocation(characterProgram, "modelViewMatrix");
	var matrixLocation = gl.getUniformLocation(aquariumProgram, "u_matrix");
	var textureLocation = gl.getUniformLocation(aquariumProgram, "u_texture");

	// Create character's buffers for vertices and colors.
	// Create aquarium's buffer for position.
	var vBuffer = gl.createBuffer();
    var cBuffer = gl.createBuffer();
	var nBuffer = gl.createBuffer();
	var positionBuffer = gl.createBuffer();
	var normalBuffer = gl.createBuffer();
	
	gl.enable(gl.DEPTH_TEST);
	
	// Bind it to ARRAY_BUFFER (think of it as ARRAY_BUFFER = positionBuffer)
	gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
  
	// Put the positions in the buffer
	setGeometry(gl);

	gl.bindBuffer(gl.ARRAY_BUFFER, normalBuffer);
	setNormals(gl);

	// Put the texture in the buffer
	// provide texture coordinates for the rectangle.
	var texcoordBuffer = setTexture(gl);

    gl.useProgram( characterProgram );

	gl.uniform4fv(gl.getUniformLocation(characterProgram, "ambientProduct"), flatten(ambientProduct));
    gl.uniform4fv(gl.getUniformLocation(characterProgram, "diffuseProduct"), flatten(diffuseProduct));
    gl.uniform4fv(gl.getUniformLocation(characterProgram, "specularProduct"), flatten(specularProduct));
    gl.uniform4fv(gl.getUniformLocation(characterProgram, "pointLightPosition"), flatten(pointLightPosition));
	gl.uniform1f(gl.getUniformLocation(characterProgram, "shininess"), shininess);
	
    gl.uniform4fv(gl.getUniformLocation(characterProgram, "dirLightIntensity"), flatten(dirLightIntensity));
    gl.uniform4fv(gl.getUniformLocation(characterProgram, "dirLightDirection"), flatten(dirLightDirection));

    gl.useProgram( aquariumProgram );

	gl.uniform4fv(gl.getUniformLocation(aquariumProgram, "ambientProduct"), flatten(ambientProduct));
    gl.uniform4fv(gl.getUniformLocation(aquariumProgram, "diffuseProduct"), flatten(diffuseProduct));
    gl.uniform4fv(gl.getUniformLocation(aquariumProgram, "specularProduct"), flatten(specularProduct));
    gl.uniform4fv(gl.getUniformLocation(aquariumProgram, "pointLightPosition"), flatten(pointLightPosition));
	gl.uniform1f(gl.getUniformLocation(aquariumProgram, "shininess"), shininess);

    // Compute the projection matrix
	var fieldOfViewRadians = degToRad(60)
	var aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
	var projectionMatrix = m4.perspective(fieldOfViewRadians, aspect, 1, 2000);
	
    //gl.useProgram(shadowProgramBg);

    //gl.bindBuffer(gl.ARRAY_BUFFER, vBuffer);
    //var vPositionShadowBg = gl.getAttribLocation( shadowProgramBg, "vPosition" );
    //gl.vertexAttribPointer(vPositionShadowBg, 4, gl.FLOAT, false, 0, 0);
    //gl.enableVertexAttribArray(vPositionShadowBg);

    //shadowBgMatrixLoc = gl.getUniformLocation(shadowProgramBg, "modelViewMatrix");
    //gl.uniformMatrix4fv(gl.getUniformLocation(shadowProgramBg, "projectionMatrix"), false, flatten(projectionMatrix));
    //gl.uniform4fv(gl.getUniformLocation(shadowProgramBg, "pointLightPosition"), flatten(pointLightPosition));
	
    //gl.useProgram(shadowProgramFloor);

    //gl.bindBuffer(gl.ARRAY_BUFFER, vBuffer);
    //var vPositionShadowFloor = gl.getAttribLocation(shadowProgramFloor, "vPosition" );
    //gl.vertexAttribPointer(vPositionShadowFloor, 4, gl.FLOAT, false, 0, 0);
    //gl.enableVertexAttribArray(vPositionShadowFloor);

    //shadowFloorMatrixLoc = gl.getUniformLocation(shadowProgramFloor, "modelViewMatrix");
    //gl.uniformMatrix4fv(gl.getUniformLocation(shadowProgramFloor, "projectionMatrix"), false, flatten(projectionMatrix));
    //gl.uniform4fv(gl.getUniformLocation(shadowProgramFloor, "pointLightPosition"), flatten(pointLightPosition));

	//--------------------------------------------------------------------------	
	
	var demo = true;
	var cameraMode = 0;
	var currentFrame = 0;
	var aquariumRotX = 0;
	var aquariumRotY = 0;
	var aquariumRotZ = 0;
	var aquariumZoom = 60;
	var drawMode = gl.TRIANGLES;
	var danceMoveTheta = SSP_SQUILLIAM_DANCE;
	var garyShakeTheta = GARY_COLLIDE_NEWTON;
	var numberOfJellyfish = 1;
	var play = true;

	var lightPosX = 0;
	var lightPosY = 0;
	var lightPosZ = 0;
	
	document.getElementById("demo-button").onclick = function(event){
		demo = !demo;
		if(demo){
			window.location.reload(true); 
		} else {
			document.getElementsByClassName("interactive")[0].style= "display: inline-block;";
			document.getElementsByClassName("interactive")[1].style= "display: inline-block;";
			document.getElementsByClassName("interactive")[2].style= "display: inline-block;";
		}
	}
	document.getElementById("camera-mode-selection").onchange = function(event){
		cameraMode = event.target.selectedIndex;	
	}
	document.getElementById("x-angle").oninput = function(event) {
        aquariumRotX = event.target.value;
    };
    document.getElementById("y-angle").oninput = function(event) {
        aquariumRotY = event.target.value;
    };
    document.getElementById("z-angle").oninput = function(event) {
        aquariumRotZ = event.target.value;
    };
	document.getElementById("x-lightpos").oninput = function(event) {
		lightPosX = event.target.value;
		pointLightPosition = vec4(lightPosX, lightPosY, lightPosZ, 0.0);
    };
    document.getElementById("y-lightpos").oninput = function(event) {
        lightPosY = event.target.value;
		pointLightPosition = vec4(lightPosX, lightPosY, lightPosZ, 0.0);
    };
    document.getElementById("z-lightpos").oninput = function(event) {
        lightPosZ = event.target.value;
		pointLightPosition = vec4(lightPosX, lightPosY, lightPosZ, 0.0);
    };
	document.getElementById("aquarium-zoom").oninput = function(event) {
        aquariumZoom = event.target.value;
    };
	document.getElementById("draw-mode-selection").onchange = function(event){
		var selection = event.target.selectedIndex;	
		if (selection == 0){
			drawMode = gl.TRIANGLES;
		} else if (selection == 1){
			drawMode = gl.LINES;
		}
	}
	document.getElementById("dance-move-selection").onchange = function(event){
		var selection = event.target.selectedIndex;
		if (selection == 0){
			danceMoveTheta = SSP_SQUILLIAM_DANCE;
		} else if (selection == 1){
			danceMoveTheta = SSP_MOONWALK;
		}
	}
	document.getElementById("gary-shake-selection").onchange = function(event){
		var selection = event.target.selectedIndex;	
		if (selection == 0){
			garyShakeTheta = GARY_COLLIDE_NEWTON;
		} else if (selection == 1){
			garyShakeTheta = GARY_LINEAR_NEWTON;
		}
	}
	document.getElementById("gary-shake-selection").onchange = function(event){
		var selection = event.target.selectedIndex;	
		if (selection == 0){
			garyShakeTheta = GARY_COLLIDE_NEWTON;
		} else if (selection == 1){
			garyShakeTheta = GARY_LINEAR_NEWTON;
		}
	}
	document.getElementById("jellyfish-number-selection").onchange = function(event){
		numberOfJellyfish = event.target.selectedIndex;	
	}
	document.getElementById("play-button").onclick = function(event){
		play = !play;
	}
	
	requestAnimationFrame(render);
	
	function render(){

		if (currentFrame >= 14 && play) {
			currentFrame = 0;
		}

		webglUtils.resizeCanvasToDisplaySize(gl.canvas);

		// Tell WebGL how to convert from clip space to pixels
		gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
		
		// Clear the canvas AND the depth buffer.
		gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

		//--------------------------------------------------------------------------
		 
		gl.useProgram (characterProgram);
		gl.disable(gl.CULL_FACE);
		
		gl.uniform4fv(gl.getUniformLocation(characterProgram, "pointLightPosition"), flatten(pointLightPosition));

		gl.bindBuffer( gl.ARRAY_BUFFER, nBuffer );
		gl.bufferData( gl.ARRAY_BUFFER, flatten(normals), gl.STATIC_DRAW );
		
		gl.vertexAttribPointer( vNormal, 3, gl.FLOAT, false, 0, 0 );
		gl.enableVertexAttribArray( vNormal );
		
		gl.bindBuffer( gl.ARRAY_BUFFER, vBuffer );
		gl.bufferData( gl.ARRAY_BUFFER, flatten(points), gl.STATIC_DRAW );
		
		gl.vertexAttribPointer( vPosition, 4, gl.FLOAT, false, 0, 0 );
		gl.enableVertexAttribArray( vPosition );
		
		gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer );
		gl.bufferData( gl.ARRAY_BUFFER, flatten(colors), gl.STATIC_DRAW );
		
		gl.vertexAttribPointer( vColor, 4, gl.FLOAT, false, 0, 0 );
		gl.enableVertexAttribArray( vColor );
		
		var cameraPosition = [0, 0, aquariumZoom];
		var up = [0, 1, 0];
		var target = [0, 0, 0];
		
		if (cameraMode == 0){
			
			cameraPosition = [0, 0, aquariumZoom];
			target = [0, 0, 0];
			
			document.getElementById("interactive-global-camera").style= "";
			
		} else if (cameraMode == 1){
			
			cameraPosition = [danceMoveTheta[currentFrame][3], -15 + danceMoveTheta[currentFrame][4], 5];
			target = [0, -15, 10];
			
			document.getElementById("interactive-global-camera").style= "display: none;";
			
		} else if (cameraMode == 2){
			
			cameraPosition = [-10 + danceMoveTheta[currentFrame][3], -15 + danceMoveTheta[currentFrame][4], 5];
			target = [-10, -15, 10];
			
			document.getElementById("interactive-global-camera").style= "display: none;";
			
		} else if (cameraMode == 3){			
			cameraPosition = [10 + danceMoveTheta[currentFrame][3], -15 + danceMoveTheta[currentFrame][4], 5];
			target = [10, -15, 10];
			
			document.getElementById("interactive-global-camera").style= "display: none;";
		
		}
		
		// Compute the camera's matrix using look at.
		var cameraMatrix = m4.lookAt(cameraPosition, target, up);

		// Make a view matrix from the camera matrix.
		var viewMatrix = m4.inverse(cameraMatrix);

		var viewProjectionMatrix = m4.multiply(projectionMatrix, viewMatrix);
    
		gl.uniformMatrix4fv( gl.getUniformLocation(characterProgram, "projectionMatrix"),  false, viewProjectionMatrix );
		
		if (numberOfJellyfish > 0){
			drawJellyfish(gl, drawMode, modelViewMatrixLoc, JELLYFISH_SWIMMING[currentFrame], 0, 0, 0, aquariumRotX, aquariumRotY, aquariumRotZ );
			if (numberOfJellyfish > 1) {
				drawJellyfish(gl, drawMode, modelViewMatrixLoc, JELLYFISH_SWIMMING[currentFrame], -10, 0, 0, aquariumRotX, aquariumRotY, aquariumRotZ );	
				if(numberOfJellyfish > 2){
					drawJellyfish(gl, drawMode, modelViewMatrixLoc, JELLYFISH_SWIMMING[currentFrame], 10, 0, 0, aquariumRotX, aquariumRotY, aquariumRotZ );
				}					
			}			
		}
		
		drawGary(gl, drawMode, modelViewMatrixLoc, garyShakeTheta[currentFrame], -10, -12.5, 7.5, aquariumRotX, aquariumRotY, aquariumRotZ );
		drawPatrick(gl, drawMode, modelViewMatrixLoc, danceMoveTheta[currentFrame], -10, -15, 0, aquariumRotX, aquariumRotY, aquariumRotZ );		
		drawSpongebob(gl, drawMode, modelViewMatrixLoc, danceMoveTheta[currentFrame], 0, -15, 0, aquariumRotX, aquariumRotY, aquariumRotZ);
		drawSquidward(gl, drawMode, modelViewMatrixLoc, danceMoveTheta[currentFrame], 10, -15, -10, aquariumRotX, aquariumRotY, aquariumRotZ );

        //gl.useProgram(shadowProgramBg);
        //gl.uniformMatrix4fv(gl.getUniformLocation(shadowProgramBg, "projectionMatrix"),  false, flatten(projectionMatrix));

        //gl.useProgram(shadowProgramFloor);
        //gl.uniformMatrix4fv(gl.getUniformLocation(shadowProgramFloor, "projectionMatrix"), false, flatten(projectionMatrix));
		
		//--------------------------------------------------------------------------
		
		// Tell it to use our aquariumProgram (pair of shaders)
		gl.useProgram(aquariumProgram);
		
		gl.enable(gl.CULL_FACE);
		gl.cullFace(gl.FRONT);
		
		gl.uniform4fv(gl.getUniformLocation(aquariumProgram, "pointLightPosition"), flatten(pointLightPosition));
		
		gl.bindBuffer( gl.ARRAY_BUFFER, normalBuffer );
		
		gl.vertexAttribPointer( vNormal2, 3, gl.FLOAT, false, 0, 0 );
		gl.enableVertexAttribArray( vNormal2 );

		// Turn on the position attribute
		gl.enableVertexAttribArray(positionLocation);

		// Bind the position buffer.
		gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

		// Tell the position attribute how to get data out of positionBuffer (ARRAY_BUFFER)
		var size = 3;          // 3 components per iteration
		var type = gl.FLOAT;   // the data is 32bit floats
		var normalize = false; // don't normalize the data
		var stride = 0;        // 0 = move forward size * sizeof(type) each iteration to get the next position
		var offset = 0;        // start at the beginning of the buffer
		gl.vertexAttribPointer(positionLocation, size, type, normalize, stride, offset);

		// Turn on the texcoord attribute
		gl.enableVertexAttribArray(texcoordLocation);

		// bind the texcoord buffer.
		gl.bindBuffer(gl.ARRAY_BUFFER, texcoordBuffer);

		// Tell the texcoord attribute how to get data out of texcoordBuffer (ARRAY_BUFFER)
		var size = 2;          // 2 components per iteration
		var type = gl.FLOAT;   // the data is 32bit floats
		var normalize = false; // don't normalize the data
		var stride = 0;        // 0 = move forward size * sizeof(type) each iteration to get the next position
		var offset = 0;        // start at the beginning of the buffer
		gl.vertexAttribPointer(texcoordLocation, size, type, normalize, stride, offset);

		var matrix = m4.xRotate(viewProjectionMatrix, -degToRad(aquariumRotX));
		matrix = m4.yRotate(matrix, -degToRad(aquariumRotY));
		matrix = m4.zRotate(matrix, -degToRad(aquariumRotZ));

		// Set the matrix.
		gl.uniformMatrix4fv(matrixLocation, false, matrix);

		// Tell the shader to use texture unit 0 for u_texture
		gl.uniform1i(textureLocation, 0);

		// Draw the geometry.
		gl.drawArrays(drawMode, 0, 6 * 6);

		if (play){
			currentFrame++;
		}
		
		setTimeout(function() { requestAnimationFrame(render); }, 80);
	}
}

/**
export function drawShadow(t, shadowProgramBg, shadowProgramFloor, shadowBgMatrixLoc, shadowFloorMatrixLoc, characterProgram) {
    gl.useProgram(shadowProgramBg);
    gl.uniformMatrix4fv(shadowBgMatrixLoc, false, flatten(t));
    gl.uniform4fv(gl.getUniformLocation(shadowProgramBg, "pointLightPosition"), flatten(pointLightPosition));
    gl.drawArrays(gl.TRIANGLES, 0, 36);

    gl.useProgram(shadowProgramFloor);
    gl.uniformMatrix4fv(shadowFloorMatrixLoc, false, flatten(t));
    gl.uniform4fv(gl.getUniformLocation(shadowProgramFloor, "pointLightPosition"), flatten(pointLightPosition));
    gl.drawArrays(gl.TRIANGLES, 0, 36);

    gl.useProgram(characterProgram);
}
*/