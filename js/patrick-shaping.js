/**
 * Various public (exported) and private (unexported)
 * method to draw Patrick hiearchically.
 *
 * @module patrick-shaping.
 */

"use strict";

/**
 * Import required variable and method from basic-shaping module.
 */
import { exportedColors, exportedPoints, exportedNormals, drawCube} from "./basic-shaping.js";
import { drawTrapezoid, drawTrapezoid2, drawTrapezoidFlip, drawTrapezoidFlip2} from "./basic-shaping.js";
import { drawTriangle, drawTriangleFlip, resetModelViewMatrixToBody, setupDraw } from "./basic-shaping.js";

/**
 * Re-export variable colors and points imported 
 * from basic-shaping module to fill the color and vertex buffer.
 */
export var colors = exportedColors;
export var points = exportedPoints;
export var normals = exportedNormals;

var t;

/** 
 * Index of theta array which define the position and rotation
 * of main body and also the rotation of each limb.
 */
const ROT_X = 0;
const ROT_Y = 1;
const ROT_Z = 2;
const POS_X = 3;
const POS_Y = 4;
const POS_Z = 5;
//const HEAD = 6;
const UPPER_ARM_R = 7;
const LOWER_ARM_R = 8;
const HAND_R = 9;
const UPPER_ARM_L = 10;
const LOWER_ARM_L = 11;
const HAND_L = 12;
const UPPER_LEG_R = 13;
const LOWER_LEG_R = 14;
const FOOT_R = 15;
const UPPER_LEG_L = 16;
const LOWER_LEG_L = 17;
const FOOT_L = 18;

/**
 * Required constants to draw Patrick's body parts.
 */ 
var BODY_WIDTH = 6.9;
var BODY_HEIGHT = 9.2;

var UPPER_ARM_WIDTH = 1.2;
var UPPER_ARM_HEIGHT = 3.2;

var LOWER_ARM_WIDTH = 1.2;
var LOWER_ARM_HEIGHT = 2;

var HAND_WIDTH = 1.2;
var HAND_HEIGHT = 0.6;

var UPPER_LEG_WIDTH = 1.7;
var UPPER_LEG_HEIGHT = 1.4;

var LOWER_LEG_WIDTH = 1.2;
var LOWER_LEG_HEIGHT = 2.4;

/**
 * Draw Patrick's main body.
 * 
 * @param { WebGLRenderingContext } gl pass WebGL rendering
 *         context to use this method.
 * @param { enum } drawMode Fill with gl.LINES of gl.TRIANGLES.
 * @param { matrix } modelViewMatrix pass the current modelViewMatrix of
 *        the basic shape.
 * @param { number } modelViewMatrixLoc pass the location of modelViewMatrix
 *        in the buffer.
 */
function body(gl, drawMode, modelViewMatrix, modelViewMatrixLoc) {
    t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, BODY_WIDTH, BODY_HEIGHT - 1.3, 2.05, 0, 2.2, 0);
    drawTriangleFlip(gl, drawMode, 9, t);
    t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, BODY_WIDTH, 2.2, 2.05, 0, -2.8, 0);
    drawTrapezoid2(gl, drawMode, 10, t);
	
    t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 1.0, 0.8, 0.5, 0.6, 3.2, 0.4);
    drawTrapezoidFlip(gl, drawMode, 0, t);
    t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 1.0, 0.8, 0.5, 0.6, 2.4, 0.4);
	drawTrapezoid(gl, drawMode, 0, t);
    t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 1.0, 0.8, 0.5, -0.6, 3.2, 0.4);
    drawTrapezoidFlip(gl, drawMode, 0, t);
    t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 1.0, 0.8, 0.5, -0.6, 2.4, 0.4);
    drawTrapezoid(gl, drawMode, 0, t);
	t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 0.4, 0.4, 0.4, 0.6, 2.6, 0.55);
    drawCube(gl, drawMode, 1, t);
    t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 0.4, 0.4, 0.4, -0.6, 2.6, 0.55);
    drawCube(gl, drawMode, 1, t);
}

/**
 * Draw Patrick's upper arm.
 * 
 * @param { WebGLRenderingContext } gl pass WebGL rendering
 *         context to use this method.
 * @param { enum } drawMode Fill with gl.LINES of gl.TRIANGLES.
 * @param { matrix } modelViewMatrix pass the current modelViewMatrix of
 *        the basic shape.
 * @param { number } modelViewMatrixLoc pass the location of modelViewMatrix
 *        in the buffer.
 */
function upperArm(gl, drawMode, modelViewMatrix, modelViewMatrixLoc) {
    t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, UPPER_ARM_WIDTH, UPPER_ARM_HEIGHT, UPPER_ARM_WIDTH, 0, 0.5 * UPPER_ARM_HEIGHT, 0);
    drawTrapezoid(gl, drawMode, 9, t);
}

/**
 * Draw Patrick's lower arm.
 * 
 * @param { WebGLRenderingContext } gl pass WebGL rendering
 *         context to use this method.
 * @param { enum } drawMode Fill with gl.LINES of gl.TRIANGLES.
 * @param { matrix } modelViewMatrix pass the current modelViewMatrix of
 *        the basic shape.
 * @param { number } modelViewMatrixLoc pass the location of modelViewMatrix
 *        in the buffer.
 */
function lowerArm(gl, drawMode, modelViewMatrix, modelViewMatrixLoc) {
    t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, LOWER_ARM_WIDTH, LOWER_ARM_HEIGHT, LOWER_ARM_WIDTH, 0, 0.5 * LOWER_ARM_HEIGHT, 0);
    drawTrapezoid(gl, drawMode, 9, t);
}

/**
 * Draw Patrick's hand.
 * 
 * @param { WebGLRenderingContext } gl pass WebGL rendering
 *         context to use this method.
 * @param { enum } drawMode Fill with gl.LINES of gl.TRIANGLES.
 * @param { matrix } modelViewMatrix pass the current modelViewMatrix of
 *        the basic shape.
 * @param { number } modelViewMatrixLoc pass the location of modelViewMatrix
 *        in the buffer.
 */
function hand(gl, drawMode, modelViewMatrix, modelViewMatrixLoc) {
    t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, HAND_WIDTH, HAND_HEIGHT, 0.5, 0, 0.5 * HAND_HEIGHT, 0);
    drawTrapezoidFlip(gl, drawMode, 9, t);
}

/**
 * Draw Patrick's upper leg.
 * 
 * @param { WebGLRenderingContext } gl pass WebGL rendering
 *         context to use this method.
 * @param { enum } drawMode Fill with gl.LINES of gl.TRIANGLES.
 * @param { matrix } modelViewMatrix pass the current modelViewMatrix of
 *        the basic shape.
 * @param { number } modelViewMatrixLoc pass the location of modelViewMatrix
 *        in the buffer.
 */
function upperLeg(gl, drawMode, modelViewMatrix, modelViewMatrixLoc) {
    t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, UPPER_LEG_WIDTH, UPPER_LEG_HEIGHT, 1, 0, 1.9 * (UPPER_LEG_HEIGHT - 0.4), 0);
    drawTrapezoidFlip(gl, drawMode, 10, t);
}

/**
 * Draw Patrick's lower leg.
 * 
 * @param { WebGLRenderingContext } gl pass WebGL rendering
 *         context to use this method.
 * @param { enum } drawMode Fill with gl.LINES of gl.TRIANGLES.
 * @param { matrix } modelViewMatrix pass the current modelViewMatrix of
 *        the basic shape.
 * @param { number } modelViewMatrixLoc pass the location of modelViewMatrix
 *        in the buffer.
 */
function lowerLeg(gl, drawMode, modelViewMatrix, modelViewMatrixLoc) {
    t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, LOWER_LEG_WIDTH, LOWER_LEG_HEIGHT, 1, 0, LOWER_LEG_HEIGHT - 0.4, 0);
    drawTriangleFlip(gl, drawMode, 9, t);
}

/**
 * Call this function in index.js' render() function to draw jellyfish.
 *
 * @param { WebGLRenderingContext } gl pass WebGL rendering
 *         context to use this method.
 * @param { enum } drawMode Fill with gl.LINES of gl.TRIANGLES.
 * @param { number } modelViewMatrixLoc pass the location of modelViewMatrix
 *        in the buffer.
 * @param { array [][] } position and rotation data for a specific animation 
          frame called theta.
 * @param { number } initialPosX current x position.
 * @param { number } initialPosY current y position.
 * @param { number } initialPosZ current z position. 
 * @param { number } initialRotX current x rotation.
 * @param { number } initialRotY current y rotation. 
 * @param { number } initialRotZ current z rotation.
 */
export function drawPatrick(gl, drawMode, modelViewMatrixLoc, theta, initialPosX, initialPosY, initialPosZ, initialRotX, initialRotY, initialRotZ){
	// Main Body
	var modelViewMatrix = resetModelViewMatrixToBody(modelViewMatrix, theta[POS_X], theta[POS_Y], theta[POS_Z], theta[ROT_X], theta[ROT_Y], theta[ROT_Z], initialPosX, initialPosY, initialPosZ, initialRotX, initialRotY, initialRotZ);
    body(gl, drawMode, modelViewMatrix, modelViewMatrixLoc);
	
	// Right Upper Arm
    modelViewMatrix = mult(modelViewMatrix, translate(1, 0.2, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[UPPER_ARM_R], 0, 0, 1));
    upperArm(gl, drawMode, modelViewMatrix, modelViewMatrixLoc);
	
	// Right Lower Arm
    modelViewMatrix = mult(modelViewMatrix, translate(0, UPPER_ARM_HEIGHT - 0.2, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[LOWER_ARM_R], 0, 0, 1));
    lowerArm(gl, drawMode, modelViewMatrix, modelViewMatrixLoc);

	// Right Hand
    modelViewMatrix = mult(modelViewMatrix, translate(0, LOWER_ARM_HEIGHT - 0.2, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[HAND_R], 0, 0, 1));
    hand(gl, drawMode, modelViewMatrix, modelViewMatrixLoc);
    
	// Left Upper Arm
    modelViewMatrix = resetModelViewMatrixToBody(modelViewMatrix, theta[POS_X], theta[POS_Y], theta[POS_Z], theta[ROT_X], theta[ROT_Y], theta[ROT_Z], initialPosX, initialPosY, initialPosZ, initialRotX, initialRotY, initialRotZ);
    modelViewMatrix = mult(modelViewMatrix, translate(-1, 0.2, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[UPPER_ARM_L], 0, 0, 1));
    upperArm(gl, drawMode, modelViewMatrix, modelViewMatrixLoc);

	// Left Lower Arm
    modelViewMatrix = mult(modelViewMatrix, translate(0, UPPER_ARM_HEIGHT - 0.2, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[LOWER_ARM_L], 0, 0, 1));
    lowerArm(gl, drawMode, modelViewMatrix, modelViewMatrixLoc);

	// Left Hand
    modelViewMatrix = mult(modelViewMatrix, translate(0, LOWER_ARM_HEIGHT - 0.2, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[HAND_L], 0, 0, 1));
    hand(gl, drawMode, modelViewMatrix, modelViewMatrixLoc);
    
	// Right Upper Leg
    modelViewMatrix = resetModelViewMatrixToBody(modelViewMatrix, theta[POS_X], theta[POS_Y], theta[POS_Z], theta[ROT_X], theta[ROT_Y], theta[ROT_Z], initialPosX, initialPosY, initialPosZ, initialRotX, initialRotY, initialRotZ);
    modelViewMatrix = mult(modelViewMatrix, translate(0.35 * BODY_WIDTH * 0.5, -2.6, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[UPPER_LEG_R], 0, 0, 1));
    upperLeg(gl, drawMode, modelViewMatrix, modelViewMatrixLoc);

	// Right Lower Leg
    modelViewMatrix = mult(modelViewMatrix, translate(0, UPPER_LEG_HEIGHT + 0.41, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[LOWER_LEG_R], 0, 0, 1));
    lowerLeg(gl, drawMode, modelViewMatrix, modelViewMatrixLoc);

	// Left Upper Leg
    modelViewMatrix = resetModelViewMatrixToBody(modelViewMatrix, theta[POS_X], theta[POS_Y], theta[POS_Z], theta[ROT_X], theta[ROT_Y], theta[ROT_Z], initialPosX, initialPosY, initialPosZ, initialRotX, initialRotY, initialRotZ);
    modelViewMatrix = mult(modelViewMatrix, translate(-0.35 * BODY_WIDTH * 0.5, -2.6, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[UPPER_LEG_L], 0, 0, 1));
    upperLeg(gl, drawMode, modelViewMatrix, modelViewMatrixLoc);

	// Left Lower Leg
    modelViewMatrix = mult(modelViewMatrix, translate(0, UPPER_LEG_HEIGHT + 0.41, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[LOWER_LEG_L], 0, 0, 1));
    lowerLeg(gl, drawMode, modelViewMatrix, modelViewMatrixLoc);	
}