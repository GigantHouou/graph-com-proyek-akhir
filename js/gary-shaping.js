/**
 * Various public (exported) and private (unexported)
 * method to draw Gary hiearchically.
 *
 * @module gary-shaping.
 */

"use strict";

/**
 * Import required variable and method from basic-shaping module.
 */
import { exportedColors, exportedPoints, exportedNormals, drawCube} from "./basic-shaping.js";
import { drawTrapezoid, drawTrapezoid2, drawTrapezoidFlip, drawTrapezoidFlip2} from "./basic-shaping.js";
import { drawTriangle, drawTriangleFlip, resetModelViewMatrixToBody, setupDraw } from "./basic-shaping.js";

/**
 * Re-export variable colors and points imported 
 * from basic-shaping module to fill the color and vertex buffer.
 */
export var colors = exportedColors;
export var points = exportedPoints;
export var normals = exportedNormals;

var t;

/** 
 * Index of theta array which define the position and rotation
 * of main body and also the rotation of each limb.
 */
const ROT_X = 0;
const ROT_Y = 1;
const ROT_Z = 2;
const POS_X = 3;
const POS_Y = 4;
const POS_Z = 5;
const EYE_R = 6;
const EYE_L = 7;

/**
 * Required constants to draw Gary's body parts.
 */ 
const BODY_WIDTH = 5.4;

const EYE_ARM_WIDTH = 0.3;
const EYE_ARM_HEIGHT = 3.2;

/**
 * Draw Gary's main body.
 * 
 * @param { WebGLRenderingContext } gl pass WebGL rendering
 *         context to use this method.
 * @param { enum } drawMode Fill with gl.LINES of gl.TRIANGLES.
 * @param { matrix } modelViewMatrix pass the current modelViewMatrix of
 *        the basic shape.
 * @param { number } modelViewMatrixLoc pass the location of modelViewMatrix
 *        in the buffer.
 */
function body(gl, drawMode, modelViewMatrix, modelViewMatrixLoc) {
	t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, BODY_WIDTH + 1.0, 0.3, 2.5, 0, -6.95, 0);
    drawCube(gl, drawMode, 11, t);
	t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, BODY_WIDTH + 1.0, 0.5, 2.5, 0, -6.6, 0);
    drawCube(gl, drawMode, 12, t);
	
	t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 5.0, 2.7, 2.5, -1.23, -3.00, 0);
    drawTrapezoidFlip(gl, drawMode, 13, t);
    t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 5.0, 2.0, 2.5, -1.23, -5.35, 0);
    drawTrapezoid(gl, drawMode, 13, t);

	t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 0.6 * (BODY_WIDTH + 1.3), 1.8, 2.4, 1.8, -5.5, 0);
    drawTrapezoidFlip(gl, drawMode, 12, t);
}

/**
 * Draw Gary's eyeball and limb connecting the eyeball and gary's head.
 *
 * @param { WebGLRenderingContext } gl pass WebGL rendering
 *         context to use this method.
 * @param { enum } drawMode Fill with gl.LINES of gl.TRIANGLES.
 * @param { matrix } modelViewMatrix pass the current modelViewMatrix of
 *        the basic shape.
 * @param { number } modelViewMatrixLoc pass the location of modelViewMatrix
 *        in the buffer.
 */
function eye(gl, drawMode, modelViewMatrix, modelViewMatrixLoc) {
	t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, EYE_ARM_WIDTH, EYE_ARM_HEIGHT, EYE_ARM_WIDTH, 0, 0.5 * EYE_ARM_HEIGHT, 0);
    drawCube(gl, drawMode, 12, t);
	
	t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 1.4, 0.8, 0.8, 0, 4.4, 0);
    drawTrapezoidFlip(gl, drawMode, 0, t);
    t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 1.4, 0.8, 0.8, 0, 3.6, 0);
    drawTrapezoid(gl, drawMode, 0, t);
	t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 0.4, 0.4, 0.6, 0, 4.0, 0.25);
    drawCube(gl, drawMode, 1, t);
}

/**
 * Call this function in index.js' render() function to draw Gary.
 *
 * @param { WebGLRenderingContext } gl pass WebGL rendering
 *         context to use this method.
 * @param { enum } drawMode Fill with gl.LINES of gl.TRIANGLES.
 * @param { number } modelViewMatrixLoc pass the location of modelViewMatrix
 *        in the buffer.
 * @param { array [][] } position and rotation data for a specific animation 
          frame called theta.
 * @param { number } initialPosX current x position.
 * @param { number } initialPosY current y position.
 * @param { number } initialPosZ current z position. 
 * @param { number } initialRotX current x rotation.
 * @param { number } initialRotY current y rotation. 
 * @param { number } initialRotZ current z rotation.
 */
export function drawGary(gl, drawMode, modelViewMatrixLoc, theta, initialPosX, initialPosY, initialPosZ, initialRotX, initialRotY, initialRotZ){
	// Main Body
	var modelViewMatrix = resetModelViewMatrixToBody(modelViewMatrix, theta[POS_X], theta[POS_Y], theta[POS_Z], theta[ROT_X], theta[ROT_Y], theta[ROT_Z], initialPosX, initialPosY, initialPosZ, initialRotX, initialRotY, initialRotZ);
    body(gl, drawMode, modelViewMatrix, modelViewMatrixLoc);

	// Right Eye
    modelViewMatrix = mult(modelViewMatrix, translate(2.6, -4.8, -0.7));
    modelViewMatrix = mult(modelViewMatrix, rotate(90, 0, 1, 0));
	modelViewMatrix = mult(modelViewMatrix, rotate(theta[EYE_R], 0, 0, 1));
    eye(gl, drawMode, modelViewMatrix, modelViewMatrixLoc);
	
	// Left Eye
	modelViewMatrix = resetModelViewMatrixToBody(modelViewMatrix, theta[POS_X], theta[POS_Y], theta[POS_Z], theta[ROT_X], theta[ROT_Y], theta[ROT_Z], initialPosX, initialPosY, initialPosZ, initialRotX, initialRotY, initialRotZ);
    modelViewMatrix = mult(modelViewMatrix, translate(2.6, -4.8, 0.7));
	modelViewMatrix = mult(modelViewMatrix, rotate(90, 0, 1, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[EYE_L], 0, 0, 1));
    eye(gl, drawMode, modelViewMatrix, modelViewMatrixLoc);
}