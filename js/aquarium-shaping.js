/**
 * Various public (exported) and private (unexported)
 * method to draw an aquarium.
 *
 * @module aquarium-shaping
 */

"use strict";

/**
 * File location of Texture map used to generate
 * aquarium's texture.
 */
const TEXTURE_IMAGE_LOCATION = "textures/edit-aquarium-texture-map/aquarium-texture-map.png"

/**
 * Width of our aquarium.
 */
const AQUARIUM_WIDTH = 20;

/**
 * Math function to convert degree to radian.
 *
 * @param { number } an integer.
 * @return { boolean } true if value is a power of 2.
 */
function isPowerOf2(value) {
	return (value & (value - 1)) === 0;
}

/**
 * Function to convert x-axis spatial coordinate to x-axis texture coordinate.
 * Formula:
 *         https://webglfundamentals.org/webgl/lessons/webgl-3d-textures.html
 *
 * @param { number } x x-axis spatial coordinate of a point in 
 *        texture map.
 * @param { number } imageWidth width of texture image.
 * @return { number } x converted into x-axis texture coordinate.
 */

var convertXToTextcoord = function(x, imageWidth){
	return x / (imageWidth - 1);
}
	
/**
 * Function to convert y-axis spatial coordinate to y-axis texture coordinate.
 * Formula:
 *        https://webglfundamentals.org/webgl/lessons/webgl-3d-textures.html
 *
 * @param { number } y y-axis spatial coordinate of a point in 
 *        texture map.  
 * @param { number } imageHeight height of texture image.
 * @return { number } y converted into y-axis texture coordinate.
 */
var convertYToTextcoord = function(y, imageHeight){
	return y / (imageHeight - 1);
}

/**
 * Math function to convert degree to radian.
 *
 * @param { number } d value of angle in degree.
 * @return { number } value of angle in radian.
 */
export function degToRad(d) {
		return d * Math.PI / 180;
}

/**
 * Fill the buffer with the values that define a cube,
 * the shape of our aquarium.
 * 
 * @param { WebGLRenderingContext } gl pass WebGL rendering
 *        context to use this method.
 */
export function setGeometry(gl) {
	var positions = new Float32Array(
		[
			-AQUARIUM_WIDTH, -AQUARIUM_WIDTH,  -AQUARIUM_WIDTH,
    		-AQUARIUM_WIDTH,  AQUARIUM_WIDTH,  -AQUARIUM_WIDTH,
    		 AQUARIUM_WIDTH, -AQUARIUM_WIDTH,  -AQUARIUM_WIDTH,
    		-AQUARIUM_WIDTH,  AQUARIUM_WIDTH,  -AQUARIUM_WIDTH,
    		 AQUARIUM_WIDTH,  AQUARIUM_WIDTH,  -AQUARIUM_WIDTH,
    		 AQUARIUM_WIDTH, -AQUARIUM_WIDTH,  -AQUARIUM_WIDTH,

    		-AQUARIUM_WIDTH, -AQUARIUM_WIDTH,   AQUARIUM_WIDTH,
    		 AQUARIUM_WIDTH, -AQUARIUM_WIDTH,   AQUARIUM_WIDTH,
    		-AQUARIUM_WIDTH,  AQUARIUM_WIDTH,   AQUARIUM_WIDTH,
    		-AQUARIUM_WIDTH,  AQUARIUM_WIDTH,   AQUARIUM_WIDTH,
    		 AQUARIUM_WIDTH, -AQUARIUM_WIDTH,   AQUARIUM_WIDTH,
    		 AQUARIUM_WIDTH,  AQUARIUM_WIDTH,   AQUARIUM_WIDTH,

    		-AQUARIUM_WIDTH,   AQUARIUM_WIDTH, -AQUARIUM_WIDTH,
    		-AQUARIUM_WIDTH,   AQUARIUM_WIDTH,  AQUARIUM_WIDTH,
    		 AQUARIUM_WIDTH,   AQUARIUM_WIDTH, -AQUARIUM_WIDTH,
    		-AQUARIUM_WIDTH,   AQUARIUM_WIDTH,  AQUARIUM_WIDTH,
    		 AQUARIUM_WIDTH,   AQUARIUM_WIDTH,  AQUARIUM_WIDTH,
    		 AQUARIUM_WIDTH,   AQUARIUM_WIDTH, -AQUARIUM_WIDTH,

    		-AQUARIUM_WIDTH,  -AQUARIUM_WIDTH, -AQUARIUM_WIDTH,
    		 AQUARIUM_WIDTH,  -AQUARIUM_WIDTH, -AQUARIUM_WIDTH,
    		-AQUARIUM_WIDTH,  -AQUARIUM_WIDTH,  AQUARIUM_WIDTH,
    		-AQUARIUM_WIDTH,  -AQUARIUM_WIDTH,  AQUARIUM_WIDTH,
    		 AQUARIUM_WIDTH,  -AQUARIUM_WIDTH, -AQUARIUM_WIDTH,
    		 AQUARIUM_WIDTH,  -AQUARIUM_WIDTH,  AQUARIUM_WIDTH,

    		-AQUARIUM_WIDTH,  -AQUARIUM_WIDTH, -AQUARIUM_WIDTH,
    		-AQUARIUM_WIDTH,  -AQUARIUM_WIDTH,  AQUARIUM_WIDTH,
    		-AQUARIUM_WIDTH,   AQUARIUM_WIDTH, -AQUARIUM_WIDTH,
    		-AQUARIUM_WIDTH,  -AQUARIUM_WIDTH,  AQUARIUM_WIDTH,
    		-AQUARIUM_WIDTH,   AQUARIUM_WIDTH,  AQUARIUM_WIDTH,
    		-AQUARIUM_WIDTH,   AQUARIUM_WIDTH, -AQUARIUM_WIDTH,

    		 AQUARIUM_WIDTH,  -AQUARIUM_WIDTH, -AQUARIUM_WIDTH,
    		 AQUARIUM_WIDTH,   AQUARIUM_WIDTH, -AQUARIUM_WIDTH,
    		 AQUARIUM_WIDTH,  -AQUARIUM_WIDTH,  AQUARIUM_WIDTH,
    		 AQUARIUM_WIDTH,  -AQUARIUM_WIDTH,  AQUARIUM_WIDTH,
    		 AQUARIUM_WIDTH,   AQUARIUM_WIDTH, -AQUARIUM_WIDTH,
     		 AQUARIUM_WIDTH,   AQUARIUM_WIDTH,  AQUARIUM_WIDTH,

    	]);
	gl.bufferData(gl.ARRAY_BUFFER, positions, gl.STATIC_DRAW);
}

export function setNormals(gl) {
	var normals = new Float32Array(
		[			
    		0, 0, -1,
			0, 0, -1,
			0, 0, -1,
			0, 0, -1,
			0, 0, -1,
			0, 0, -1,

			0, 0, 1,
			0, 0, 1,
			0, 0, 1,
			0, 0, 1,
			0, 0, 1,
			0, 0, 1,

			0, 1, 0,
			0, 1, 0,
			0, 1, 0,
			0, 1, 0,
			0, 1, 0,
			0, 1, 0,
			
    		0, -1, 0,
			0, -1, 0,
			0, -1, 0,
			0, -1, 0,
			0, -1, 0,
			0, -1, 0,

    		-1, 0, 0,
			-1, 0, 0,
			-1, 0, 0,
			-1, 0, 0,
			-1, 0, 0,
			-1, 0, 0,

    		1, 0, 0,
			1, 0, 0,
			1, 0, 0,
			1, 0, 0,
			1, 0, 0,
			1, 0, 0,
    	]);
	gl.bufferData(gl.ARRAY_BUFFER, normals, gl.STATIC_DRAW);
}

/**
 * Set aquarium's texture. 
 *
 * @param { WebGLRenderingContext } gl pass WebGL rendering
 *        context to use this method.
 * @return Texture coordinate buffer.
 */
export function setTexture(gl){
	// Create and bind texture.
	var texture = gl.createTexture();
	gl.bindTexture(gl.TEXTURE_2D, texture);
  
	// Fill the texture with a 1x1 blue pixel.
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE, new Uint8Array([0, 0, 255, 255]));
  
	// Asynchronously load an image with promise
	// Source: https://stackoverflow.com/questions/29516390/how-to-access-the-value-of-a-promise
	//         https://stackoverflow.com/a/52060802
	let promise = new Promise(function(resolve) {
		
		var image = new Image();
		image.src = TEXTURE_IMAGE_LOCATION;
		image.addEventListener('load', function() {
			// Now that the image has loaded make copy it to the texture.
			gl.bindTexture(gl.TEXTURE_2D, texture);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA,gl.UNSIGNED_BYTE, image);

			// Check if the image is a power of 2 in both dimensions.
			if (isPowerOf2(image.width) && isPowerOf2(image.height)) {
			
				// Yes, it's a power of 2. Generate mips.
				gl.generateMipmap(gl.TEXTURE_2D);
    
			} else {
				// No, it's not a power of 2. Turn of mips and set wrapping to clamp to edge
				gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
				gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
				gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
			}
			resolve(image);
		});
	});
		
	// provide texture coordinates for the rectangle.
	var texcoordBuffer = gl.createBuffer();
	
	// Get loaded image resolved by promise.
	promise.then(function(image){
	
		gl.bindBuffer(gl.ARRAY_BUFFER, texcoordBuffer);
	
		// Set Texcoords: Fill the buffer with texture coordinates the cube.
		gl.bufferData(
			gl.ARRAY_BUFFER,
				new Float32Array(
					[
					// Select texture bikini-bottom-background from texture map.
					convertXToTextcoord(1167 * 2, image.width), convertYToTextcoord(1167 * 1, image.height),
					convertXToTextcoord(1167 * 2, image.width), convertYToTextcoord(1167 * 2, image.height),
					convertXToTextcoord(1167 * 3, image.width), convertYToTextcoord(1167 * 1, image.height),
					convertXToTextcoord(1167 * 2, image.width), convertYToTextcoord(1167 * 2, image.height),
					convertXToTextcoord(1167 * 3, image.width), convertYToTextcoord(1167 * 2, image.height),
					convertXToTextcoord(1167 * 3, image.width), convertYToTextcoord(1167 * 1, image.height),
        
					// Select texture frosted-glass-center from texture map.
					convertXToTextcoord(1167 * 0, image.width), convertYToTextcoord(1167 * 1, image.height),
					convertXToTextcoord(1167 * 1, image.width), convertYToTextcoord(1167 * 1, image.height),
					convertXToTextcoord(1167 * 0, image.width), convertYToTextcoord(1167 * 2, image.height),
					convertXToTextcoord(1167 * 0, image.width), convertYToTextcoord(1167 * 2, image.height),
					convertXToTextcoord(1167 * 1, image.width), convertYToTextcoord(1167 * 1, image.height),
					convertXToTextcoord(1167 * 1, image.width), convertYToTextcoord(1167 * 2, image.height),
				
					//  Select texture bikin-bottom-sky from texture map.
					convertXToTextcoord(1167 * 2, image.width), convertYToTextcoord(1167 * 2, image.height),
					convertXToTextcoord(1167 * 2, image.width), convertYToTextcoord(1167 * 3, image.height),
					convertXToTextcoord(1167 * 3, image.width), convertYToTextcoord(1167 * 2, image.height),
					convertXToTextcoord(1167 * 2, image.width), convertYToTextcoord(1167 * 3, image.height),
					convertXToTextcoord(1167 * 3, image.width), convertYToTextcoord(1167 * 3, image.height),
					convertXToTextcoord(1167 * 3, image.width), convertYToTextcoord(1167 * 2, image.height),
        
					//  Select texture sand-floor from texture map.
					convertXToTextcoord(1167 * 2, image.width), convertYToTextcoord(1167 * 0, image.height),
					convertXToTextcoord(1167 * 3, image.width), convertYToTextcoord(1167 * 0, image.height),
					convertXToTextcoord(1167 * 2, image.width), convertYToTextcoord(1167 * 1, image.height),
					convertXToTextcoord(1167 * 2, image.width), convertYToTextcoord(1167 * 1, image.height),
					convertXToTextcoord(1167 * 3, image.width), convertYToTextcoord(1167 * 0, image.height),
					convertXToTextcoord(1167 * 3, image.width), convertYToTextcoord(1167 * 1, image.height),
				
					//  Select texture frosted-glass-right from texture map.
					convertXToTextcoord(1167 * 3, image.width), convertYToTextcoord(1167 * 1, image.height),
					convertXToTextcoord(1167 * 3, image.width), convertYToTextcoord(1167 * 2, image.height),
					convertXToTextcoord(1167 * 4, image.width), convertYToTextcoord(1167 * 1, image.height),
					convertXToTextcoord(1167 * 3, image.width), convertYToTextcoord(1167 * 2, image.height),
					convertXToTextcoord(1167 * 4, image.width), convertYToTextcoord(1167 * 2, image.height),
					convertXToTextcoord(1167 * 4, image.width), convertYToTextcoord(1167 * 1, image.height),
				
					//  Select texture frosted-glass-left from texture map.
					convertXToTextcoord(1167 * 1, image.width), convertYToTextcoord(1167 * 1, image.height),
					convertXToTextcoord(1167 * 2, image.width), convertYToTextcoord(1167 * 1, image.height),
					convertXToTextcoord(1167 * 1, image.width), convertYToTextcoord(1167 * 2, image.height),
					convertXToTextcoord(1167 * 1, image.width), convertYToTextcoord(1167 * 2, image.height),
					convertXToTextcoord(1167 * 2, image.width), convertYToTextcoord(1167 * 1, image.height),
					convertXToTextcoord(1167 * 2, image.width), convertYToTextcoord(1167 * 2, image.height),
		]),
		gl.STATIC_DRAW);
	});

	return texcoordBuffer;
}