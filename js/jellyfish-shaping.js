/**
 * Various public (exported) and private (unexported)
 * method to draw jellyfish hiearchically.
 *
 * @module jellyfish-shaping.
 */
 
"use strict";

/**
 * Import required variable and method from basic-shaping module.
 */
import { exportedColors, exportedPoints, exportedNormals, drawCube} from "./basic-shaping.js";
import { drawTrapezoid, drawTrapezoid2, drawTrapezoidFlip, drawTrapezoidFlip2} from "./basic-shaping.js";
import { drawTriangle, drawTriangleFlip, resetModelViewMatrixToBody, setupDraw } from "./basic-shaping.js";

/**
 * Re-export variable colors and points imported 
 * from basic-shaping module to fill the color and vertex buffer.
 */
export var colors = exportedColors;
export var points = exportedPoints;
export var normals = exportedNormals;

var t;

/** 
 * Index of theta array which define the position and rotation
 * of main body and also the rotation of each limb.
 */
const ROT_X = 0;
const ROT_Y = 1;
const ROT_Z = 2;
const POS_X = 3;
const POS_Y = 4;
const POS_Z = 5;
const FRONT_UPPER_TENTACLE_R = 6;
const FRONT_LOWER_TENTACLE_R = 7;
const FRONT_TIP_TENTACLE_R = 8;
const FRONT_UPPER_TENTACLE_L = 9;
const FRONT_LOWER_TENTACLE_L = 10;
const FRONT_TIP_TENTACLE_L = 11;
const MIDDLE_UPPER_TENTACLE_R = 12;
const MIDDLE_LOWER_TENTACLE_R = 13;
const MIDDLE_TIP_TENTACLE_R = 14;
const MIDDLE_UPPER_TENTACLE_L = 15;
const MIDDLE_LOWER_TENTACLE_L = 16;
const MIDDLE_TIP_TENTACLE_L = 17;
const BACK_UPPER_TENTACLE_R = 18;
const BACK_LOWER_TENTACLE_R = 19;
const BACK_TIP_TENTACLE_R = 20;
const BACK_UPPER_TENTACLE_L = 21;
const BACK_LOWER_TENTACLE_L = 22;
const BACK_TIP_TENTACLE_L = 23;

/**
 * Required constants to draw jellyfish's body parts.
 */ 
const BODY_WIDTH = 5.4;
const BODY_HEIGHT = 7.2;

const UPPER_TENTACLE_WIDTH = 0.5;
const UPPER_TENTACLE_HEIGHT = 2.6;

const LOWER_TENTACLE_WIDTH = 0.6;
const LOWER_TENTACLE_HEIGHT = 1.6;

const TIP_WIDTH = 0.9;
const TIP_HEIGHT = 0.9;

/**
 * Draw jellyfish's main body.
 * 
 * @param { WebGLRenderingContext } gl pass WebGL rendering
 *         context to use this method.
 * @param { enum } drawMode Fill with gl.LINES of gl.TRIANGLES.
 * @param { matrix } modelViewMatrix pass the current modelViewMatrix of
 *        the basic shape.
 * @param { number } modelViewMatrixLoc pass the location of modelViewMatrix
 *        in the buffer.
 */
function body(gl, drawMode, modelViewMatrix, modelViewMatrixLoc) {
	t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 3.0, 0.5, 5.0, 0, 0.00, 0);
	drawCube(gl, drawMode, 15, t);
	t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 4.5, 0.5, 4.5, 0, 0.00, 0);
	drawCube(gl, drawMode, 14, t);
	t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 5.0, 0.5, 3.0, 0, 0.00, 0);
	drawCube(gl, drawMode, 15, t);
	
	t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 2.5, 0.5, 4.5, 0, 0.50, 0);
	drawCube(gl, drawMode, 15, t);
	t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 3.5, 0.5, 3.5, 0, 0.50, 0);
	drawCube(gl, drawMode, 14, t);
	t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 4.5, 0.5, 2.5, 0, 0.50, 0);
	drawCube(gl, drawMode, 15, t);
	
	t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 2.0, 0.5, 4.0, 0, 1.00, 0);
	drawCube(gl, drawMode, 15, t);
	t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 3.0, 0.5, 3.0, 0, 1.00, 0);
	drawCube(gl, drawMode, 14, t);
	t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 4.0, 0.5, 2.0, 0, 1.00, 0);
	drawCube(gl, drawMode, 15, t);
	
	t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 1.5, 0.5, 3.5, 0, 1.50, 0);
	drawCube(gl, drawMode, 15, t);
	t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 2.5, 0.5, 2.5, 0, 1.50, 0);
	drawCube(gl, drawMode, 14, t);
	t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 3.5, 0.5, 1.5, 0, 1.50, 0);
	drawCube(gl, drawMode, 15, t);
	
	t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 1.5, 0.75, 3.5, 0, 2.1, 0);
	drawTrapezoidFlip(gl, drawMode, 15, t);
	setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 3.5, 0.75, 1.5, 0, 2.1, 0);
	drawTrapezoidFlip(gl, drawMode, 15, t);
	
	t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 3.2, 0.75, 5.2, 0, -0.60, 0);
	drawTrapezoid(gl, drawMode, 15, t);
	t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 5.2, 0.75, 3.2, 0, -0.60, 0);
	drawTrapezoid(gl, drawMode, 15, t);
}

/**
 * Draw jellyfish's upper tentacle.
 *
 * @param { WebGLRenderingContext } gl pass WebGL rendering
 *         context to use this method.
 * @param { enum } drawMode Fill with gl.LINES of gl.TRIANGLES.
 * @param { matrix } modelViewMatrix pass the current modelViewMatrix of
 *        the basic shape.
 * @param { number } modelViewMatrixLoc pass the location of modelViewMatrix
 *        in the buffer.
 */
function upperTentacle(gl, drawMode, modelViewMatrix, modelViewMatrixLoc) {
    t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, UPPER_TENTACLE_WIDTH, UPPER_TENTACLE_HEIGHT, UPPER_TENTACLE_WIDTH, 0, 0.5 * UPPER_TENTACLE_HEIGHT, 0);
    drawTrapezoid(gl, drawMode, 15, t);
}

/**
 * Draw jellyfish's lower tentacle.
 *
 * @param { WebGLRenderingContext } gl pass WebGL rendering
 *         context to use this method.
 * @param { enum } drawMode Fill with gl.LINES of gl.TRIANGLES.
 * @param { matrix } modelViewMatrix pass the current modelViewMatrix of
 *        the basic shape.
 * @param { number } modelViewMatrixLoc pass the location of modelViewMatrix
 *        in the buffer.
 */
function lowerTentacle(gl, drawMode, modelViewMatrix, modelViewMatrixLoc) {
    t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, LOWER_TENTACLE_WIDTH, LOWER_TENTACLE_HEIGHT, LOWER_TENTACLE_WIDTH, 0, 0.5 * LOWER_TENTACLE_HEIGHT, 0);
    drawTrapezoid(gl, drawMode, 15, t);
}

/**
 * Draw jellyfish's tip of tentacle.
 *
 * @param { WebGLRenderingContext } gl pass WebGL rendering
 *         context to use this method.
 * @param { enum } drawMode Fill with gl.LINES of gl.TRIANGLES.
 * @param { matrix } modelViewMatrix pass the current modelViewMatrix of
 *        the basic shape.
 * @param { number } modelViewMatrixLoc pass the location of modelViewMatrix
 *        in the buffer.
 */
function tipTentacle(gl, drawMode, modelViewMatrix, modelViewMatrixLoc) {
    t = setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, TIP_WIDTH, TIP_HEIGHT, TIP_WIDTH, 0, 0.5 * TIP_HEIGHT, 0);
    drawTrapezoidFlip2(gl, drawMode, 15, t);
}

/**
 * Call this function in index.js' render() function to draw jellyfish.
 *
 * @param { WebGLRenderingContext } gl pass WebGL rendering
 *         context to use this method.
 * @param { enum } drawMode Fill with gl.LINES of gl.TRIANGLES.
 * @param { number } modelViewMatrixLoc pass the location of modelViewMatrix
 *        in the buffer.
 * @param { array [][] } position and rotation data for a specific animation 
          frame called theta.
 * @param { number } initialPosX current x position.
 * @param { number } initialPosY current y position.
 * @param { number } initialPosZ current z position. 
 * @param { number } initialRotX current x rotation.
 * @param { number } initialRotY current y rotation. 
 * @param { number } initialRotZ current z rotation.
 */
export function drawJellyfish(gl, drawMode, modelViewMatrixLoc, theta, initialPosX, initialPosY, initialPosZ, initialRotX, initialRotY, initialRotZ){
	// Main Body
	var modelViewMatrix = resetModelViewMatrixToBody(modelViewMatrix, theta[POS_X], theta[POS_Y], theta[POS_Z], theta[ROT_X], theta[ROT_Y], theta[ROT_Z], initialPosX, initialPosY, initialPosZ, initialRotX, initialRotY, initialRotZ);
    body(gl, drawMode, modelViewMatrix, modelViewMatrixLoc);
	
	// Front Right Tentacle
	modelViewMatrix = resetModelViewMatrixToBody(modelViewMatrix, theta[POS_X], theta[POS_Y], theta[POS_Z], theta[ROT_X], theta[ROT_Y], theta[ROT_Z], initialPosX, initialPosY, initialPosZ, initialRotX, initialRotY, initialRotZ);
    modelViewMatrix = mult(modelViewMatrix, translate(0.2 * BODY_WIDTH * 0.6,  -0.8, 1.5));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[FRONT_UPPER_TENTACLE_R], 0, 0, 1));
    upperTentacle(gl, drawMode, modelViewMatrix, modelViewMatrixLoc);

    modelViewMatrix = mult(modelViewMatrix, translate(0, LOWER_TENTACLE_HEIGHT + 0.8, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[FRONT_LOWER_TENTACLE_R], 0, 0, 1));
    lowerTentacle(gl, drawMode, modelViewMatrix, modelViewMatrixLoc);

    modelViewMatrix = mult(modelViewMatrix, translate(0, TIP_HEIGHT + 0.5, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[FRONT_TIP_TENTACLE_R], 0, 0, 1));
    tipTentacle(gl, drawMode, modelViewMatrix, modelViewMatrixLoc);
	
	// Front Left Tentacle
	modelViewMatrix = resetModelViewMatrixToBody(modelViewMatrix, theta[POS_X], theta[POS_Y], theta[POS_Z], theta[ROT_X], theta[ROT_Y], theta[ROT_Z], initialPosX, initialPosY, initialPosZ, initialRotX, initialRotY, initialRotZ);
    modelViewMatrix = mult(modelViewMatrix, translate(-0.2 * BODY_WIDTH * 0.6, -0.8, 1.5));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[FRONT_UPPER_TENTACLE_L], 0, 0, 1));
    upperTentacle(gl, drawMode, modelViewMatrix, modelViewMatrixLoc);

    modelViewMatrix = mult(modelViewMatrix, translate(0, LOWER_TENTACLE_HEIGHT + 0.8, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[FRONT_LOWER_TENTACLE_L], 0, 0, 1));
    lowerTentacle(gl, drawMode, modelViewMatrix, modelViewMatrixLoc);

    modelViewMatrix = mult(modelViewMatrix, translate(0, TIP_HEIGHT + 0.5, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[FRONT_TIP_TENTACLE_L], 0, 0, 1));
    tipTentacle(gl, drawMode, modelViewMatrix, modelViewMatrixLoc);
	
	// Middle Right Tentacle
    modelViewMatrix = resetModelViewMatrixToBody(modelViewMatrix, theta[POS_X], theta[POS_Y], theta[POS_Z], theta[ROT_X], theta[ROT_Y], theta[ROT_Z], initialPosX, initialPosY, initialPosZ, initialRotX, initialRotY, initialRotZ);
    modelViewMatrix = mult(modelViewMatrix, translate(0.5 * BODY_WIDTH * 0.6, -0.8, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[MIDDLE_UPPER_TENTACLE_R], 0, 0, 1));
    upperTentacle(gl, drawMode, modelViewMatrix, modelViewMatrixLoc);

    modelViewMatrix = mult(modelViewMatrix, translate(0, LOWER_TENTACLE_HEIGHT + 0.8, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[MIDDLE_LOWER_TENTACLE_R], 0, 0, 1));
    lowerTentacle(gl, drawMode, modelViewMatrix, modelViewMatrixLoc);

    modelViewMatrix = mult(modelViewMatrix, translate(0, TIP_HEIGHT + 0.5, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[MIDDLE_TIP_TENTACLE_R], 0, 0, 1));
    tipTentacle(gl, drawMode, modelViewMatrix, modelViewMatrixLoc);
	
	// Middle Left Tentacle
    modelViewMatrix = resetModelViewMatrixToBody(modelViewMatrix, theta[POS_X], theta[POS_Y], theta[POS_Z], theta[ROT_X], theta[ROT_Y], theta[ROT_Z], initialPosX, initialPosY, initialPosZ, initialRotX, initialRotY, initialRotZ);
    modelViewMatrix = mult(modelViewMatrix, translate(-0.5 * BODY_WIDTH * 0.6, -0.8, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[MIDDLE_UPPER_TENTACLE_L], 0, 0, 1));
    upperTentacle(gl, drawMode, modelViewMatrix, modelViewMatrixLoc);

    modelViewMatrix = mult(modelViewMatrix, translate(0, LOWER_TENTACLE_HEIGHT + 0.8, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[MIDDLE_LOWER_TENTACLE_L], 0, 0, 1));
    lowerTentacle(gl, drawMode, modelViewMatrix, modelViewMatrixLoc);

    modelViewMatrix = mult(modelViewMatrix, translate(0, TIP_HEIGHT + 0.5, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[MIDDLE_TIP_TENTACLE_L], 0, 0, 1));
    tipTentacle(gl, drawMode, modelViewMatrix, modelViewMatrixLoc);
	
	// Back Right Tentacle
	modelViewMatrix = resetModelViewMatrixToBody(modelViewMatrix, theta[POS_X], theta[POS_Y], theta[POS_Z], theta[ROT_X], theta[ROT_Y], theta[ROT_Z], initialPosX, initialPosY, initialPosZ, initialRotX, initialRotY, initialRotZ);
    modelViewMatrix = mult(modelViewMatrix, translate(0.2 * BODY_WIDTH * 0.6, -0.8, -1.5));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[BACK_UPPER_TENTACLE_R], 0, 0, 1));
    upperTentacle(gl, drawMode, modelViewMatrix, modelViewMatrixLoc);

    modelViewMatrix = mult(modelViewMatrix, translate(0, LOWER_TENTACLE_HEIGHT + 0.8, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[BACK_LOWER_TENTACLE_R], 0, 0, 1));
    lowerTentacle(gl, drawMode, modelViewMatrix, modelViewMatrixLoc);

    modelViewMatrix = mult(modelViewMatrix, translate(0, TIP_HEIGHT + 0.5, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[BACK_TIP_TENTACLE_R], 0, 0, 1));
    tipTentacle(gl, drawMode, modelViewMatrix, modelViewMatrixLoc);
	
	// Back Left Tentacle
	modelViewMatrix = resetModelViewMatrixToBody(modelViewMatrix, theta[POS_X], theta[POS_Y], theta[POS_Z], theta[ROT_X], theta[ROT_Y], theta[ROT_Z], initialPosX, initialPosY, initialPosZ, initialRotX, initialRotY, initialRotZ);
    modelViewMatrix = mult(modelViewMatrix, translate(-0.2 * BODY_WIDTH * 0.6, -0.8, -1.5));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[BACK_UPPER_TENTACLE_L], 0, 0, 1));
    upperTentacle(gl, drawMode, modelViewMatrix, modelViewMatrixLoc);

    modelViewMatrix = mult(modelViewMatrix, translate(0, LOWER_TENTACLE_HEIGHT + 0.8, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[BACK_LOWER_TENTACLE_L], 0, 0, 1));
    lowerTentacle(gl, drawMode, modelViewMatrix, modelViewMatrixLoc);

    modelViewMatrix = mult(modelViewMatrix, translate(0, TIP_HEIGHT + 0.5, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[BACK_TIP_TENTACLE_L], 0, 0, 1));
    tipTentacle(gl, drawMode, modelViewMatrix, modelViewMatrixLoc);
}