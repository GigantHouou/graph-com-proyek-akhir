/**
 * Various public (exported) and private (unexported)
 * variable and method to draw a basic 3D shape used 
 * to generate character model hiearchically.
 *
 * @module basic-shaping.
 */

"use strict";
//import { drawShadow, gl, shadowProgramBg, shadowProgramFloor, shadowBgMatrixLoc, shadowFloorMatrixLoc, characterProgram } from "../index.js";

/**
 * Each basic 3D shape consists of 36 quad-vertices.
 */
const QUAD_NUM_VERTICES = 36;

/** 
 * Vertex data to draw a cube.
 */ 
const CUBE_VERTICES = [
	vec4( -0.5, -0.5,  0.5, 1.0 ),
    vec4( -0.5,  0.5,  0.5, 1.0 ),
    vec4(  0.5,  0.5,  0.5, 1.0 ),
    vec4(  0.5, -0.5,  0.5, 1.0 ),
    vec4( -0.5, -0.5, -0.5, 1.0 ),
    vec4( -0.5,  0.5, -0.5, 1.0 ),
    vec4(  0.5,  0.5, -0.5, 1.0 ),
    vec4(  0.5, -0.5, -0.5, 1.0 )
];

/** 
 * Vertex data to draw a type-1 trapezoid.
 */ 
const TRAPEZOID_VERTICES = [
    vec4( -0.4, -0.5,  0.4, 1.0 ),
    vec4( -0.5,  0.5,  0.5, 1.0 ),
    vec4(  0.5,  0.5,  0.5, 1.0 ),
    vec4(  0.4, -0.5,  0.4, 1.0 ),
    vec4( -0.4, -0.5, -0.4, 1.0 ),
    vec4( -0.5,  0.5, -0.5, 1.0 ),
    vec4(  0.5,  0.5, -0.5, 1.0 ),
    vec4(  0.4, -0.5, -0.4, 1.0 )
];

/** 
 * Vertex data to draw a type-2 trapezoid.
 */ 
const TRAPEZOID_2_VERTICES = [
    vec4( -0.3, -0.5,  0.3, 1.0 ),
    vec4( -0.5,  0.5,  0.5, 1.0 ),
    vec4(  0.5,  0.5,  0.5, 1.0 ),
    vec4(  0.3, -0.5,  0.3, 1.0 ),
    vec4( -0.3, -0.5, -0.3, 1.0 ),
    vec4( -0.5,  0.5, -0.5, 1.0 ),
    vec4(  0.5,  0.5, -0.5, 1.0 ),
    vec4(  0.3, -0.5, -0.3, 1.0 )
];

/** 
 * Vertex data to draw a flipped type-1 trapezoid.
 */ 
const TRAPEZOID_FLIP_VERTICES = [
    vec4( -0.5 , -0.5,  0.5 , 1.0 ),
    vec4( -0.35,  0.5,  0.35, 1.0 ),
    vec4(  0.35,  0.5,  0.35, 1.0 ),
    vec4(  0.5 , -0.5,  0.5 , 1.0 ),
    vec4( -0.5 , -0.5, -0.5 , 1.0 ),
    vec4( -0.35,  0.5, -0.35, 1.0 ),
    vec4(  0.35,  0.5, -0.35, 1.0 ),
    vec4(  0.5 , -0.5, -0.5 , 1.0 )
];

/** 
 * Vertex data to draw a flipped type-2 trapezoid.
 */ 
const TRAPEZOID_FLIP_2_VERTICES = [
    vec4( -0.5, -0.5,  0.5, 1.0 ),
    vec4( -0.2,  0.5,  0.2, 1.0 ),
    vec4(  0.2,  0.5,  0.2, 1.0 ),
    vec4(  0.5, -0.5,  0.5, 1.0 ),
    vec4( -0.5, -0.5, -0.5, 1.0 ),
    vec4( -0.2,  0.5, -0.2, 1.0 ),
    vec4(  0.2,  0.5, -0.2, 1.0 ),
    vec4(  0.5, -0.5, -0.5, 1.0 )
];

/** 
 * Vertex data to draw a pyramid.
 */ 
const TRIANGLE_VERTICES = [
    vec4( -0.05, -0.5,  0.05, 1.0 ),
    vec4( -0.5,  0.5,  0.5, 1.0 ),
    vec4(  0.5,  0.5,  0.5, 1.0 ),
    vec4(  0.05, -0.5,  0.05, 1.0 ),
    vec4( -0.05, -0.5, -0.05, 1.0 ),
    vec4( -0.5,  0.5, -0.5, 1.0 ),
    vec4(  0.5,  0.5, -0.5, 1.0 ),
    vec4(  0.05, -0.5, -0.05, 1.0 )
];

/** 
 * Vertex data to draw a flipped pyramid.
 */ 
const TRIANGLE_FLIP_VERTICES = [
    vec4( -0.5, -0.5,  0.5, 1.0 ),
    vec4( -0.05,  0.5,  0.05, 1.0 ),
    vec4(  0.05,  0.5,  0.05, 1.0 ),
    vec4(  0.5, -0.5,  0.5, 1.0 ),
    vec4( -0.5, -0.5, -0.5, 1.0 ),
    vec4( -0.05,  0.5, -0.05, 1.0 ),
    vec4(  0.05,  0.5, -0.05, 1.0 ),
    vec4(  0.5, -0.5, -0.5, 1.0 )
];

/**
 * Lists all vertex data for filling the 
 * vertex buffer
 */
const QUAD_VERTICES_LIST = [
    CUBE_VERTICES,
    TRAPEZOID_VERTICES,
    TRAPEZOID_2_VERTICES,
    TRAPEZOID_FLIP_VERTICES,
    TRAPEZOID_FLIP_2_VERTICES,
    TRIANGLE_VERTICES,
    TRIANGLE_FLIP_VERTICES,
];

/**
 * Lists all color data (RED, GREEN, BLUE, ALPHA)
 * for filling the color buffer.
 */
const VERTEX_COLORS = [

	vec4( 246/255, 246/255, 246/255, 1.0 ), // 0: Eye's sclera and Spongebob's shirt.
	vec4(   2/255,   2/255,  10/255, 1.0 ), // 1: Eye's pupil, Spongebob's shoes, and Squidward's shirt (edge).
		
	vec4( 237/255, 228/255, 25/255, 1.0 ),  // 2: Spongebob's skin
    vec4( 180/255, 31/255, 16/255, 1.0 ),   // 3: Spongebob's tie and Spongebob's mouth.
	vec4( 129/255, 75/255, 34/255, 1.0 ),   // 4: Spongebob's squarepants
    vec4( 208/255, 200/255, 18/255, 1.0 ),  // 5: Spongebob's nose
   
	vec4( 167/255, 205/255, 187/255, 1.0 ), // 6: Squidward's skin
    vec4( 161/255,  16/255, 189/255, 1.0 ), // 7: Squidward's shirt (middle)
    vec4( 134/255, 178/255, 157/255, 1.0 ), // 8: Squidward's nose

	vec4( 245/255, 165/255, 166/255, 1.0),  // 9: Patrick's skin (Pink)
	vec4( 176/255, 226/255,  33/255, 1.0),  // 10: Patrick's pants (Green)

	vec4( 222/255, 234/255, 186/255, 1.0 ), // 11: Gary's foot
	vec4( 136/255, 202/255, 230/255, 1.0 ), // 12: Gary's body
	vec4( 249/255, 197/255, 194/255, 1.0 ), // 13: Gary's shell
	
	vec4( 160/255,  39/255,  84/255, 1.0 ), // 14: Pink jellyfish's spot
	vec4( 219/255, 132/255, 207/255, 1.0 )  // 15: Pink jellfish's body
];

/**
 * Freeze all nested object to make it a true
 * constants.
 */ 
Object.freeze(CUBE_VERTICES);
Object.freeze(TRAPEZOID_VERTICES);
Object.freeze(TRAPEZOID_2_VERTICES);
Object.freeze(TRAPEZOID_FLIP_VERTICES);
Object.freeze(TRAPEZOID_FLIP_2_VERTICES);
Object.freeze(TRIANGLE_VERTICES);
Object.freeze(TRIANGLE_FLIP_VERTICES);
Object.freeze(QUAD_VERTICES_LIST);
Object.freeze(VERTEX_COLORS);

/**
 * Define and generate arrays for filling 
 * the color and vertex buffer.
 */
export var exportedColors = [];
export var exportedPoints = [];
export var exportedNormals = [];
colorQuads();

/**
 * Convert a width, height, depth of basic shape.
 * into a mat4.
 *
 * Notes: Remove when scale in MV.js supports scale matrices
 *
 * @param { number } a width of basic shape.
 * @param { number } b height of basic shape. 
 * @param { number } c depth of basic shape
 * @return { number } width, height, depth data converted into mat4
 */
function scale4(a, b, c) {
   var result = mat4();
   result[0][0] = a;
   result[1][1] = b;
   result[2][2] = c;
   return result;
}

//----------------------------------------------------------------------------

/**
 * Math function to convert degree to radian.
 *
 * @param { number } d value of angle in degree.
 * @return { number } value of angle in radian.
 */
export function degToRad(d) {
		return d * Math.PI / 180;
}

/**
 * Push each vertex data with it corresponding color
 * to exportedColors and exportedPoints.
 *
 * @param { number } a index of vertices.
 * @param { number } b index of vertices.
 * @param { number } c index of vertices.
 * @param { number } d index of vertices.
 * @param { number } colorIndex index of color on VERTEX_COLORS.
 * @param { array [][] } vertices vertex data of a basic shape.
 */
function quad( a,  b,  c,  d, colorIndex, vertices ) {
    var t1 = subtract(vertices[b], vertices[a]);
    var t2 = subtract(vertices[c], vertices[b]);
    var normal = cross(t1, t2);
    normal = vec3(normal);
    normal = normalize(normal);

    exportedColors.push(VERTEX_COLORS[colorIndex]);
    exportedPoints.push(vertices[a]);
    exportedColors.push(VERTEX_COLORS[colorIndex]);
    exportedPoints.push(vertices[b]);
    exportedColors.push(VERTEX_COLORS[colorIndex]);
    exportedPoints.push(vertices[c]);
    exportedColors.push(VERTEX_COLORS[colorIndex]);
    exportedPoints.push(vertices[a]);
    exportedColors.push(VERTEX_COLORS[colorIndex]);
    exportedPoints.push(vertices[c]);
    exportedColors.push(VERTEX_COLORS[colorIndex]);
    exportedPoints.push(vertices[d]);
    
    exportedNormals.push(normal);
    exportedNormals.push(normal);
    exportedNormals.push(normal);
    exportedNormals.push(normal);
    exportedNormals.push(normal);
    exportedNormals.push(normal);
}

/**
 * Fill exportedColors and exportedPoints 2D Array.
 */
function colorQuads() {
	// For each vertex data in QUAD_VERTICES_LIST
    for (var i = 0; i < QUAD_VERTICES_LIST.length; i++) {
        // Correspond the vertex data with each color on VERTEX_COLORS
		// and push it to exportedColors and exportedPoints
		for (var j = 0; j < VERTEX_COLORS.length; j++) {
            quad( 1, 0, 3, 2, j, QUAD_VERTICES_LIST[i] );
            quad( 2, 3, 7, 6, j, QUAD_VERTICES_LIST[i] );
            quad( 3, 0, 4, 7, j, QUAD_VERTICES_LIST[i] );
            quad( 6, 5, 1, 2, j, QUAD_VERTICES_LIST[i] );
            quad( 4, 5, 6, 7, j, QUAD_VERTICES_LIST[i] );
            quad( 5, 4, 0, 1, j, QUAD_VERTICES_LIST[i] );
        }
    }
}

/**
 * Draw a cube.
 *
 * @param { WebGLRenderingContext } gl pass WebGL rendering
 *         context to use this method.
 * @param { enum } drawMode Fill with gl.LINES of gl.TRIANGLES.
 * @param { number } colorIndex iIndex of color on VERTEX_COLORS
 */
export function drawCube(gl, drawMode, colorIndex, t) {
    gl.drawArrays( drawMode, 0 + 36 * colorIndex, QUAD_NUM_VERTICES );
    //drawShadow(t, shadowProgramBg, shadowProgramFloor, shadowBgMatrixLoc, shadowFloorMatrixLoc, characterProgram);
}

/**
 * Draw a type-1 trapezoid.
 *
 * @param { WebGLRenderingContext } gl pass WebGL rendering
 *         context to use this method.
 * @param { enum } drawMode Fill with gl.LINES of gl.TRIANGLES.
 * @param { number } colorIndex index of color on VERTEX_COLORS
 */
export function drawTrapezoid(gl, drawMode, colorIndex, t) {
    gl.drawArrays( drawMode, 36 * VERTEX_COLORS.length + 36 * colorIndex, QUAD_NUM_VERTICES );
    //drawShadow(t, shadowProgramBg, shadowProgramFloor, shadowBgMatrixLoc, shadowFloorMatrixLoc, characterProgram);
}

/**
 * Draw a type-2 trapezoid.
 *
 * @param { WebGLRenderingContext } gl pass WebGL rendering
 *         context to use this method.
 * @param { enum } drawMode Fill with gl.LINES of gl.TRIANGLES.
 * @param { number } colorIndex index of color on VERTEX_COLORS
 */
export function drawTrapezoid2(gl, drawMode, colorIndex, t) {
    gl.drawArrays( drawMode, 2 * 36 * VERTEX_COLORS.length + 36 * colorIndex, QUAD_NUM_VERTICES );
    //drawShadow(t, shadowProgramBg, shadowProgramFloor, shadowBgMatrixLoc, shadowFloorMatrixLoc, characterProgram);
}

/**
 * Draw a flipped type-1 trapezoid.
 *
 * @param { WebGLRenderingContext } gl pass WebGL rendering
 *         context to use this method.
 * @param { enum } drawMode Fill with gl.LINES of gl.TRIANGLES.
 * @param { number } colorIndex index of color on VERTEX_COLORS
 */
export function drawTrapezoidFlip(gl, drawMode, colorIndex, t) {
    gl.drawArrays( drawMode, 3 * 36 * VERTEX_COLORS.length + 36 * colorIndex, QUAD_NUM_VERTICES );
    //drawShadow(t, shadowProgramBg, shadowProgramFloor, shadowBgMatrixLoc, shadowFloorMatrixLoc, characterProgram);
}

/**
 * Draw a flipped type-2 trapezoid.
 *
 * @param { WebGLRenderingContext } gl pass WebGL rendering
 *         context to use this method.
 * @param { enum } drawMode Fill with gl.LINES of gl.TRIANGLES.
 * @param { number } colorIndex index of color on VERTEX_COLORS
 */
export function drawTrapezoidFlip2(gl, drawMode, colorIndex, t) {
    gl.drawArrays( drawMode, 4 * 36 * VERTEX_COLORS.length + 36 * colorIndex, QUAD_NUM_VERTICES );
    //drawShadow(t, shadowProgramBg, shadowProgramFloor, shadowBgMatrixLoc, shadowFloorMatrixLoc, characterProgram);
}

/**
 * Draw a pyramid.
 *
 * @param { WebGLRenderingContext } gl pass WebGL rendering
 *         context to use this method.
 * @param { enum } drawMode Fill with gl.LINES of gl.TRIANGLES.
 * @param { number } colorIndex index of color on VERTEX_COLORS
 */
export function drawTriangle(gl, drawMode, colorIndex, t) {
    gl.drawArrays( drawMode, 5 * 36 * VERTEX_COLORS.length + 36 * colorIndex, QUAD_NUM_VERTICES );
    //drawShadow(t, shadowProgramBg, shadowProgramFloor, shadowBgMatrixLoc, shadowFloorMatrixLoc, characterProgram);
}

/**
 * Draw a flipped pyramid.
 *
 * @param { WebGLRenderingContext } gl pass WebGL rendering
 *         context to use this method.
 * @param { enum } drawMode Fill with gl.LINES of gl.TRIANGLES.
 * @param { number } colorIndex index of color on VERTEX_COLORS
 */
export function drawTriangleFlip(gl, drawMode, colorIndex, t) {
    gl.drawArrays( drawMode, 6 * 36 * VERTEX_COLORS.length + 36 * colorIndex, QUAD_NUM_VERTICES );
    //drawShadow(t, shadowProgramBg, shadowProgramFloor, shadowBgMatrixLoc, shadowFloorMatrixLoc, characterProgram);
}

/**
 * Before drawing any limb on hiearchical modelling, call this method to
 * reset the modelViewMatrix back to the main body.
 * 
 * @param { matrix } modelViewMatrix pass the current modelViewMatrix of
 *        the basic shape.
 * @param { number } thetaPosX current x position of main body relative to
 *        initialPosX.
 * @param { number } thetaPosY current y position of main body relative to
 *        initialPosY.
 * @param { number } thetaPosZ current z position of main body relative to
 *        initialPosY.  
 * @param { number } thetaRotX current x rotation of main body relative to
 *        initialRotX.
 * @param { number } thetaRotY current Y rotation of main body relative to
 *        initialRotY.
 * @param { number } thetaRotZ current z rotation of main body relative to
 *        initialRotZ.
 * @param { number } initialPosX current x position.
 * @param { number } initialPosY current y position.
 * @param { number } initialPosZ current z position. 
 * @param { number } initialRotX current x rotation.
 * @param { number } initialRotY current y rotation. 
 * @param { number } initialRotZ current z rotation.
 */
export function resetModelViewMatrixToBody(modelViewMatrix, thetaPosX, thetaPosY, thetaPosZ, thetaRotX, thetaRotY, thetaRotZ, initialPosX, initialPosY, initialPosZ, initialRotX, initialRotY, initialRotZ) {
    
	modelViewMatrix = rotate(thetaRotZ - initialRotZ, 0, 0, 1);

	modelViewMatrix = mult(modelViewMatrix, translate((thetaPosX + initialPosX) * Math.cos(degToRad(thetaRotY - initialRotY)), (thetaPosY + initialPosY) * Math.cos(degToRad(thetaRotX - initialRotX)), thetaPosZ + initialPosZ ))

    modelViewMatrix = mult(modelViewMatrix, rotate(thetaRotX - initialRotX, 1, 0, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(thetaRotY - initialRotY, 0, 1, 0));
 	
	return modelViewMatrix;
}

/**
 * Call this method before draw function above:
 *
 *       - drawCube(gl, drawMode, colorIndex)
 *       - drawTrapezoid(gl, drawMode, colorIndex)
 *       - drawTrapezoid2(gl, drawMode, colorIndex)
 *       - drawTrapezoidFlip(gl, drawMode, colorIndex)
 *       - drawTrapezoid2Flip(gl, drawMode, colorIndex)
 *       - drawTriangle(gl, drawMode, colorIndex)
 *       - drawTriangleFlip(gl, drawMode, colorIndex)
 *
 * to define the width, height, depth, and spatial coordinate 
 * location (x, y, z) the basic shape.
 *
 * @param { WebGLRenderingContext } gl pass WebGL rendering
 *         context to use this method.
 * @param { matrix } modelViewMatrix pass the current modelViewMatrix of
 *        the basic shape.
 * @param { number } modelViewMatrixLoc pass the location of modelViewMatrix
 *        in the buffer.
 * @param { number } width width of basic shape.
 * @param { number } height height of basic shape. 
 * @param { number } depth depth of basic shape
 * @param { number } x x-spatial coordinate of basic shape
 * @param { number } y y-spatial coordinate of basic shape
 * @param { number } z z-spatial coordinate of basic shape
 */
export function setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, width, height, depth, x, y, z) {
    var s = scale4(width, height, depth);
    var instanceMatrix = mult(translate( x, y, z), s);
    var t = mult(modelViewMatrix, instanceMatrix);
	
	// Update model view matrix in the buffer.
    gl.uniformMatrix4fv(modelViewMatrixLoc,  false, flatten(t));
    return t;
}