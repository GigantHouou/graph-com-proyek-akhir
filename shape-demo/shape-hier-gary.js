"use strict";

import { colors, points, drawGary } from "./js/gary-shaping.js";
import { ROT_X, ROT_Y, ROT_Z, POS_X, POS_Y } from "./js/gary-shaping.js";
import { EYE_R, EYE_L } from "./js/gary-shaping.js";

var gl, modelViewMatrixLoc;

var theta = [];
for (var i = 0; i < 18; i++) {
    theta.push(0);
}

//--------------------------------------------------

window.onload = function init() {

	// Get A WebGL context
	/** @type {HTMLCanvasElement} */
    var canvas = document.getElementById( "gl-canvas" );
	gl = canvas.getContext("webgl");
    if (!gl){ 
		alert("WebGL isn't available");
	}
    gl.viewport( 0, 0, canvas.width, canvas.height );

    gl.clearColor( 1, 0.9, 1, 1 );
    gl.enable( gl.DEPTH_TEST );

    //
    //  Load shaders and initialize attribute buffers
    //
    var program = initShaders( gl, "vertex-shader", "fragment-shader" );

    gl.useProgram( program );

    // Load shaders and use the resulting shader program

    program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );

    // Create and initialize  buffer objects

    var vBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, vBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(points), gl.STATIC_DRAW );

    var vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.vertexAttribPointer( vPosition, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );

    var cBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(colors), gl.STATIC_DRAW );

    var vColor = gl.getAttribLocation( program, "vColor" );
    gl.vertexAttribPointer( vColor, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vColor );
	
    document.getElementById("x-angle").oninput = function(event) {
        theta[ROT_X] = event.target.value;
    };
    document.getElementById("y-angle").oninput = function(event) {
        theta[ROT_Y] = event.target.value;
    };
    document.getElementById("z-angle").oninput = function(event) {
        theta[ROT_Z] = event.target.value;
    };
    document.getElementById("x-pos").oninput = function(event) {
        theta[POS_X] = event.target.value;
    };
    document.getElementById("y-pos").oninput = function(event) {
        theta[POS_Y] = event.target.value;
    };
    document.getElementById("eye-r").oninput = function(event) {
        theta[EYE_R] = event.target.value;
    };
	document.getElementById("eye-l").oninput = function(event) {
        theta[EYE_L] = event.target.value;
    };
	
	document.getElementById("print-slider-values").onclick = function() {
        console.log(theta.toString());
    };
	
    modelViewMatrixLoc = gl.getUniformLocation(program, "modelViewMatrix");

    var projectionMatrix = ortho(-10, 10, -10, 10, -10, 10);
    gl.uniformMatrix4fv( gl.getUniformLocation(program, "projectionMatrix"),  false, flatten(projectionMatrix) );

    render();
}

//----------------------------------------------------------------------------*/

var render = function() {

    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT );

	drawGary(gl, modelViewMatrixLoc, theta);
	
    requestAnimationFrame(render);
}