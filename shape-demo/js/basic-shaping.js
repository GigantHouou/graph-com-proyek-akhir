"use strict";

const QUAD_NUM_VERTICES = 36;

const CUBE_VERTICES = [
	vec4( -0.5, -0.5,  0.5, 1.0 ),
    vec4( -0.5,  0.5,  0.5, 1.0 ),
    vec4(  0.5,  0.5,  0.5, 1.0 ),
    vec4(  0.5, -0.5,  0.5, 1.0 ),
    vec4( -0.5, -0.5, -0.5, 1.0 ),
    vec4( -0.5,  0.5, -0.5, 1.0 ),
    vec4(  0.5,  0.5, -0.5, 1.0 ),
    vec4(  0.5, -0.5, -0.5, 1.0 )
];

const TRAPEZOID_VERTICES = [
    vec4( -0.4, -0.5,  0.4, 1.0 ),
    vec4( -0.5,  0.5,  0.5, 1.0 ),
    vec4(  0.5,  0.5,  0.5, 1.0 ),
    vec4(  0.4, -0.5,  0.4, 1.0 ),
    vec4( -0.4, -0.5, -0.4, 1.0 ),
    vec4( -0.5,  0.5, -0.5, 1.0 ),
    vec4(  0.5,  0.5, -0.5, 1.0 ),
    vec4(  0.4, -0.5, -0.4, 1.0 )
];

const TRAPEZOID_2_VERTICES = [
    vec4( -0.3, -0.5,  0.3, 1.0 ),
    vec4( -0.5,  0.5,  0.5, 1.0 ),
    vec4(  0.5,  0.5,  0.5, 1.0 ),
    vec4(  0.3, -0.5,  0.3, 1.0 ),
    vec4( -0.3, -0.5, -0.3, 1.0 ),
    vec4( -0.5,  0.5, -0.5, 1.0 ),
    vec4(  0.5,  0.5, -0.5, 1.0 ),
    vec4(  0.3, -0.5, -0.3, 1.0 )
];

const TRAPEZOID_FLIP_VERTICES = [
    vec4( -0.5 , -0.5,  0.5 , 1.0 ),
    vec4( -0.35,  0.5,  0.35, 1.0 ),
    vec4(  0.35,  0.5,  0.35, 1.0 ),
    vec4(  0.5 , -0.5,  0.5 , 1.0 ),
    vec4( -0.5 , -0.5, -0.5 , 1.0 ),
    vec4( -0.35,  0.5, -0.35, 1.0 ),
    vec4(  0.35,  0.5, -0.35, 1.0 ),
    vec4(  0.5 , -0.5, -0.5 , 1.0 )
];

const TRAPEZOID_FLIP_2_VERTICES = [
    vec4( -0.5, -0.5,  0.5, 1.0 ),
    vec4( -0.2,  0.5,  0.2, 1.0 ),
    vec4(  0.2,  0.5,  0.2, 1.0 ),
    vec4(  0.5, -0.5,  0.5, 1.0 ),
    vec4( -0.5, -0.5, -0.5, 1.0 ),
    vec4( -0.2,  0.5, -0.2, 1.0 ),
    vec4(  0.2,  0.5, -0.2, 1.0 ),
    vec4(  0.5, -0.5, -0.5, 1.0 )
];

const TRIANGLE_VERTICES = [
    vec4( -0.05, -0.5,  0.05, 1.0 ),
    vec4( -0.5,  0.5,  0.5, 1.0 ),
    vec4(  0.5,  0.5,  0.5, 1.0 ),
    vec4(  0.05, -0.5,  0.05, 1.0 ),
    vec4( -0.05, -0.5, -0.05, 1.0 ),
    vec4( -0.5,  0.5, -0.5, 1.0 ),
    vec4(  0.5,  0.5, -0.5, 1.0 ),
    vec4(  0.05, -0.5, -0.05, 1.0 )
];

const TRIANGLE_FLIP_VERTICES = [
    vec4( -0.5, -0.5,  0.5, 1.0 ),
    vec4( -0.05,  0.5,  0.05, 1.0 ),
    vec4(  0.05,  0.5,  0.05, 1.0 ),
    vec4(  0.5, -0.5,  0.5, 1.0 ),
    vec4( -0.5, -0.5, -0.5, 1.0 ),
    vec4( -0.05,  0.5, -0.05, 1.0 ),
    vec4(  0.05,  0.5, -0.05, 1.0 ),
    vec4(  0.5, -0.5, -0.5, 1.0 )
];

const QUAD_VERTICES_LIST = [
    CUBE_VERTICES,
    TRAPEZOID_VERTICES,
    TRAPEZOID_2_VERTICES,
    TRAPEZOID_FLIP_VERTICES,
    TRAPEZOID_FLIP_2_VERTICES,
    TRIANGLE_VERTICES,
    TRIANGLE_FLIP_VERTICES,
];

const VERTEX_COLORS = [

	vec4( 246/255, 246/255, 246/255, 1.0 ), // 0: Eye's sclera and Spongebob's shirt.
	vec4(   2/255,   2/255,  10/255, 1.0 ), // 1: Eye's pupil, Spongebob's shoes, and Squidward's shirt (edge).
		
	vec4( 237/255, 228/255, 25/255, 1.0 ),  // 2: Spongebob's skin
    vec4( 180/255, 31/255, 16/255, 1.0 ),   // 3: Spongebob's tie and Spongebob's mouth.
	vec4( 129/255, 75/255, 34/255, 1.0 ),   // 4: Spongebob's squarepants
    vec4( 208/255, 200/255, 18/255, 1.0 ),  // 5: Spongebob's nose
   
	vec4( 167/255, 205/255, 187/255, 1.0 ), // 6: Squidward's skin
    vec4( 161/255,  16/255, 189/255, 1.0 ), // 7: Squidward's shirt (middle)
    vec4( 134/255, 178/255, 157/255, 1.0 ), // 8: Squidward's nose

	vec4( 245/255, 165/255, 166/255, 1.0),  // 9: Patrick's skin (Pink)
	vec4( 176/255, 226/255,  33/255, 1.0),  // 10: Patrick's pants (Green)

	vec4( 222/255, 234/255, 186/255, 1.0 ), // 11: Gary's foot
	vec4( 136/255, 202/255, 230/255, 1.0 ), // 12: Gary's body
	vec4( 249/255, 197/255, 194/255, 1.0 ), // 13: Gary's shell
	
	vec4( 160/255,  39/255,  84/255, 1.0 ), // 14: Pink jellyfish's spot
	vec4( 219/255, 132/255, 207/255, 1.0 )  // 15: Pink jellfish's body
];

Object.freeze(CUBE_VERTICES);
Object.freeze(TRAPEZOID_VERTICES);
Object.freeze(TRAPEZOID_2_VERTICES);
Object.freeze(TRAPEZOID_FLIP_VERTICES);
Object.freeze(TRAPEZOID_FLIP_2_VERTICES);
Object.freeze(TRIANGLE_VERTICES);
Object.freeze(TRIANGLE_FLIP_VERTICES);
Object.freeze(QUAD_VERTICES_LIST);
Object.freeze(VERTEX_COLORS);

//----------------------------------------------------------------------------

export var exportedColors = [];
export var exportedPoints = [];

colorQuads();

//----------------------------------------------------------------------------

// Remove when scale in MV.js supports scale matrices

function scale4(a, b, c) {
   var result = mat4();
   result[0][0] = a;
   result[1][1] = b;
   result[2][2] = c;
   return result;
}

//----------------------------------------------------------------------------

function quad( a,  b,  c,  d, colorIndex, vertices ) {
    exportedColors.push(VERTEX_COLORS[colorIndex]);
    exportedPoints.push(vertices[a]);
    exportedColors.push(VERTEX_COLORS[colorIndex]);
    exportedPoints.push(vertices[b]);
    exportedColors.push(VERTEX_COLORS[colorIndex]);
    exportedPoints.push(vertices[c]);
    exportedColors.push(VERTEX_COLORS[colorIndex]);
    exportedPoints.push(vertices[a]);
    exportedColors.push(VERTEX_COLORS[colorIndex]);
    exportedPoints.push(vertices[c]);
    exportedColors.push(VERTEX_COLORS[colorIndex]);
    exportedPoints.push(vertices[d]);
}

function colorQuads() {
    for (var i = 0; i < QUAD_VERTICES_LIST.length; i++) {
        for (var j = 0; j < VERTEX_COLORS.length; j++) {
            quad( 1, 0, 3, 2, j, QUAD_VERTICES_LIST[i] );
            quad( 2, 3, 7, 6, j, QUAD_VERTICES_LIST[i] );
            quad( 3, 0, 4, 7, j, QUAD_VERTICES_LIST[i] );
            quad( 6, 5, 1, 2, j, QUAD_VERTICES_LIST[i] );
            quad( 4, 5, 6, 7, j, QUAD_VERTICES_LIST[i] );
            quad( 5, 4, 0, 1, j, QUAD_VERTICES_LIST[i] );
        }
    }
}

export function drawCube(gl, colorIndex) {
    gl.drawArrays( gl.TRIANGLES, 0 + 36 * colorIndex, QUAD_NUM_VERTICES );
}

export function drawTrapezoid(gl, colorIndex) {
    gl.drawArrays( gl.TRIANGLES, 36 * VERTEX_COLORS.length + 36 * colorIndex, QUAD_NUM_VERTICES );
}

export function drawTrapezoid2(gl, colorIndex) {
    gl.drawArrays( gl.TRIANGLES, 2 * 36 * VERTEX_COLORS.length + 36 * colorIndex, QUAD_NUM_VERTICES );
}

export function drawTrapezoidFlip(gl, colorIndex) {
    gl.drawArrays( gl.TRIANGLES, 3 * 36 * VERTEX_COLORS.length + 36 * colorIndex, QUAD_NUM_VERTICES );
}

export function drawTrapezoidFlip2(gl, colorIndex) {
    gl.drawArrays( gl.TRIANGLES, 4 * 36 * VERTEX_COLORS.length + 36 * colorIndex, QUAD_NUM_VERTICES );
}

export function drawTriangle(gl, colorIndex) {
    gl.drawArrays( gl.TRIANGLES, 5 * 36 * VERTEX_COLORS.length + 36 * colorIndex, QUAD_NUM_VERTICES );
}

export function drawTriangleFlip(gl, colorIndex) {
    gl.drawArrays( gl.TRIANGLES, 6 * 36 * VERTEX_COLORS.length + 36 * colorIndex, QUAD_NUM_VERTICES );
}

export function setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, width, height, depth, x, y, z) {
    var s = scale4(width, height, depth);
    var instanceMatrix = mult(translate( x, y, z), s);
    var t = mult(modelViewMatrix, instanceMatrix);
    gl.uniformMatrix4fv(modelViewMatrixLoc,  false, flatten(t));
}