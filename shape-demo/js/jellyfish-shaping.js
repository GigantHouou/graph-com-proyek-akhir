"use strict";

import { exportedColors, exportedPoints, drawCube} from "./basic-shaping.js";
import { drawTrapezoid, drawTrapezoid2, drawTrapezoidFlip, drawTrapezoidFlip2} from "./basic-shaping.js";
import { drawTriangle, drawTriangleFlip, setupDraw } from "./basic-shaping.js";

export var colors = exportedColors;
export var points = exportedPoints;

export const ROT_X = 0;
export const ROT_Y = 1;
export const ROT_Z = 2;
export const POS_X = 3;
export const POS_Y = 4;
export const FRONT_UPPER_TENTACLE_R = 5;
export const FRONT_LOWER_TENTACLE_R = 6;
export const FRONT_TIP_TENTACLE_R = 7;
export const FRONT_UPPER_TENTACLE_L = 8;
export const FRONT_LOWER_TENTACLE_L = 9;
export const FRONT_TIP_TENTACLE_L = 10;
export const MIDDLE_UPPER_TENTACLE_R = 11;
export const MIDDLE_LOWER_TENTACLE_R = 12;
export const MIDDLE_TIP_TENTACLE_R = 13;
export const MIDDLE_UPPER_TENTACLE_L = 14;
export const MIDDLE_LOWER_TENTACLE_L = 15;
export const MIDDLE_TIP_TENTACLE_L = 16;
export const BACK_UPPER_TENTACLE_R = 17;
export const BACK_LOWER_TENTACLE_R = 18;
export const BACK_TIP_TENTACLE_R = 19;
export const BACK_UPPER_TENTACLE_L = 20;
export const BACK_LOWER_TENTACLE_L = 21;
export const BACK_TIP_TENTACLE_L = 22;

const BODY_WIDTH = 5.4;
const BODY_HEIGHT = 7.2;

const UPPER_TENTACLE_WIDTH = 0.5;
const UPPER_TENTACLE_HEIGHT = 2.6;

const LOWER_TENTACLE_WIDTH = 0.6;
const LOWER_TENTACLE_HEIGHT = 1.6;

const TIP_WIDTH = 0.9;
const TIP_HEIGHT = 0.9;

//----------------------------------------------------------------------------

function body(gl, modelViewMatrix, modelViewMatrixLoc) {
	setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 3.0, 0.5, 5.0, 0, 0.00, 0);
	drawCube(gl, 15);
	setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 4.5, 0.5, 4.5, 0, 0.00, 0);
	drawCube(gl, 14);
	setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 5.0, 0.5, 3.0, 0, 0.00, 0);
	drawCube(gl, 15);
	
	setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 2.5, 0.5, 4.5, 0, 0.50, 0);
	drawCube(gl, 15);
	setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 3.5, 0.5, 3.5, 0, 0.50, 0);
	drawCube(gl, 14);
	setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 4.5, 0.5, 2.5, 0, 0.50, 0);
	drawCube(gl, 15);
	
	setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 2.0, 0.5, 4.0, 0, 1.00, 0);
	drawCube(gl, 15);
	setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 3.0, 0.5, 3.0, 0, 1.00, 0);
	drawCube(gl, 14);
	setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 4.0, 0.5, 2.0, 0, 1.00, 0);
	drawCube(gl, 15);
	
	setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 1.5, 0.5, 3.5, 0, 1.50, 0);
	drawCube(gl, 15);
	setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 2.5, 0.5, 2.5, 0, 1.50, 0);
	drawCube(gl, 14);
	setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 3.5, 0.5, 1.5, 0, 1.50, 0);
	drawCube(gl, 15);
	
	setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 1.5, 0.75, 3.5, 0, 2.1, 0);
	drawTrapezoidFlip(gl, 15);
	setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 3.5, 0.75, 1.5, 0, 2.1, 0);
	drawTrapezoidFlip(gl, 15);
	
	setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 3.2, 0.75, 5.2, 0, -0.60, 0);
	drawTrapezoid(gl, 15);
	setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 5.2, 0.75, 3.2, 0, -0.60, 0);
	drawTrapezoid(gl, 15);
}

//----------------------------------------------------------------------------

function upperTentacle(gl, modelViewMatrix, modelViewMatrixLoc) {
    setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, UPPER_TENTACLE_WIDTH, UPPER_TENTACLE_HEIGHT, UPPER_TENTACLE_WIDTH, 0, 0.5 * UPPER_TENTACLE_HEIGHT, 0);
    drawTrapezoid(gl, 15);
}

//----------------------------------------------------------------------------

function lowerTentacle(gl, modelViewMatrix, modelViewMatrixLoc) {
    setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, LOWER_TENTACLE_WIDTH, LOWER_TENTACLE_HEIGHT, LOWER_TENTACLE_WIDTH, 0, 0.5 * LOWER_TENTACLE_HEIGHT, 0);
    drawTrapezoid(gl, 15);
}

//----------------------------------------------------------------------------

function tipTentacle(gl, modelViewMatrix, modelViewMatrixLoc) {
    setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, TIP_WIDTH, TIP_HEIGHT, TIP_WIDTH, 0, 0.5 * TIP_HEIGHT, 0);
    drawTrapezoidFlip2(gl, 15);
}

//----------------------------------------------------------------------------

function resetModelViewMatrixToBody(modelViewMatrix, theta) {
    modelViewMatrix = rotate(0, 0, 1, 0);
    modelViewMatrix = mult(modelViewMatrix, translate(theta[POS_X], theta[POS_Y], 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[ROT_X], 1, 0, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[ROT_Y], 0, 1, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[ROT_Z], 0, 0, 1));
	return modelViewMatrix;
}

export function drawJellyfish(gl, modelViewMatrixLoc, theta){
	var modelViewMatrix = resetModelViewMatrixToBody(modelViewMatrix, theta);
    body(gl, modelViewMatrix, modelViewMatrixLoc);
	
	// Front Right Tentacle
	modelViewMatrix = resetModelViewMatrixToBody(modelViewMatrix, theta);
    modelViewMatrix = mult(modelViewMatrix, translate(0.2 * BODY_WIDTH * 0.6,  -0.8, 1.5));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[FRONT_UPPER_TENTACLE_R], 0, 0, 1));
    upperTentacle(gl, modelViewMatrix, modelViewMatrixLoc);

    modelViewMatrix = mult(modelViewMatrix, translate(0, LOWER_TENTACLE_HEIGHT + 0.8, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[FRONT_LOWER_TENTACLE_R], 0, 0, 1));
    lowerTentacle(gl, modelViewMatrix, modelViewMatrixLoc);

    modelViewMatrix = mult(modelViewMatrix, translate(0, TIP_HEIGHT + 0.5, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[FRONT_TIP_TENTACLE_R], 0, 0, 1));
    tipTentacle(gl, modelViewMatrix, modelViewMatrixLoc);
	
	// Front Left Tentacle
	modelViewMatrix = resetModelViewMatrixToBody(modelViewMatrix, theta);
    modelViewMatrix = mult(modelViewMatrix, translate(-0.2 * BODY_WIDTH * 0.6, -0.8, 1.5));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[FRONT_UPPER_TENTACLE_L], 0, 0, 1));
    upperTentacle(gl, modelViewMatrix, modelViewMatrixLoc);

    modelViewMatrix = mult(modelViewMatrix, translate(0, LOWER_TENTACLE_HEIGHT + 0.8, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[FRONT_LOWER_TENTACLE_L], 0, 0, 1));
    lowerTentacle(gl, modelViewMatrix, modelViewMatrixLoc);

    modelViewMatrix = mult(modelViewMatrix, translate(0, TIP_HEIGHT + 0.5, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[FRONT_TIP_TENTACLE_L], 0, 0, 1));
    tipTentacle(gl, modelViewMatrix, modelViewMatrixLoc);
	
	// Middle Right Tentacle
    modelViewMatrix = resetModelViewMatrixToBody(modelViewMatrix, theta);
    modelViewMatrix = mult(modelViewMatrix, translate(0.5 * BODY_WIDTH * 0.6, -0.8, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[MIDDLE_UPPER_TENTACLE_R], 0, 0, 1));
    upperTentacle(gl, modelViewMatrix, modelViewMatrixLoc);

    modelViewMatrix = mult(modelViewMatrix, translate(0, LOWER_TENTACLE_HEIGHT + 0.8, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[MIDDLE_LOWER_TENTACLE_R], 0, 0, 1));
    lowerTentacle(gl, modelViewMatrix, modelViewMatrixLoc);

    modelViewMatrix = mult(modelViewMatrix, translate(0, TIP_HEIGHT + 0.5, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[MIDDLE_TIP_TENTACLE_R], 0, 0, 1));
    tipTentacle(gl, modelViewMatrix, modelViewMatrixLoc);
	
	// Middle Left Tentacle
    modelViewMatrix = resetModelViewMatrixToBody(modelViewMatrix, theta);
    modelViewMatrix = mult(modelViewMatrix, translate(-0.5 * BODY_WIDTH * 0.6, -0.8, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[MIDDLE_UPPER_TENTACLE_L], 0, 0, 1));
    upperTentacle(gl, modelViewMatrix, modelViewMatrixLoc);

    modelViewMatrix = mult(modelViewMatrix, translate(0, LOWER_TENTACLE_HEIGHT + 0.8, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[MIDDLE_LOWER_TENTACLE_L], 0, 0, 1));
    lowerTentacle(gl, modelViewMatrix, modelViewMatrixLoc);

    modelViewMatrix = mult(modelViewMatrix, translate(0, TIP_HEIGHT + 0.5, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[MIDDLE_TIP_TENTACLE_L], 0, 0, 1));
    tipTentacle(gl, modelViewMatrix, modelViewMatrixLoc);
	
	// Back Right Tentacle
	modelViewMatrix = resetModelViewMatrixToBody(modelViewMatrix, theta);
    modelViewMatrix = mult(modelViewMatrix, translate(0.2 * BODY_WIDTH * 0.6, -0.8, -1.5));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[BACK_UPPER_TENTACLE_R], 0, 0, 1));
    upperTentacle(gl, modelViewMatrix, modelViewMatrixLoc);

    modelViewMatrix = mult(modelViewMatrix, translate(0, LOWER_TENTACLE_HEIGHT + 0.8, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[BACK_LOWER_TENTACLE_R], 0, 0, 1));
    lowerTentacle(gl, modelViewMatrix, modelViewMatrixLoc);

    modelViewMatrix = mult(modelViewMatrix, translate(0, TIP_HEIGHT + 0.5, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[BACK_TIP_TENTACLE_R], 0, 0, 1));
    tipTentacle(gl, modelViewMatrix, modelViewMatrixLoc);
	
	// Back Left Tentacle
	modelViewMatrix = resetModelViewMatrixToBody(modelViewMatrix, theta);
    modelViewMatrix = mult(modelViewMatrix, translate(-0.2 * BODY_WIDTH * 0.6, -0.8, -1.5));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[BACK_UPPER_TENTACLE_L], 0, 0, 1));
    upperTentacle(gl, modelViewMatrix, modelViewMatrixLoc);

    modelViewMatrix = mult(modelViewMatrix, translate(0, LOWER_TENTACLE_HEIGHT + 0.8, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[BACK_LOWER_TENTACLE_L], 0, 0, 1));
    lowerTentacle(gl, modelViewMatrix, modelViewMatrixLoc);

    modelViewMatrix = mult(modelViewMatrix, translate(0, TIP_HEIGHT + 0.5, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[BACK_TIP_TENTACLE_L], 0, 0, 1));
    tipTentacle(gl, modelViewMatrix, modelViewMatrixLoc);
}