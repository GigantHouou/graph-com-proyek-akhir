"use strict";

import { exportedColors, exportedPoints, drawCube} from "./basic-shaping.js";
import { drawTrapezoid, drawTrapezoid2, drawTrapezoidFlip, drawTrapezoidFlip2} from "./basic-shaping.js";
import { drawTriangle, drawTriangleFlip, setupDraw } from "./basic-shaping.js";

export var colors = exportedColors;
export var points = exportedPoints;

export const ROT_X = 0;
export const ROT_Y = 1;
export const ROT_Z = 2;
export const POS_X = 3;
export const POS_Y = 4;
export const EYE_R = 5;
export const EYE_L = 6;

const BODY_WIDTH = 5.4;

const EYE_ARM_WIDTH = 0.3;
const EYE_ARM_HEIGHT = 3.2;

//----------------------------------------------------------------------------

function body(gl, modelViewMatrix, modelViewMatrixLoc) {
	setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, BODY_WIDTH + 1.0, 0.3, 2.5, 0, -6.95, 0);
    drawCube(gl, 11);
	setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, BODY_WIDTH + 1.0, 0.5, 2.5, 0, -6.6, 0);
    drawCube(gl, 12);
	
	setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 5.0, 2.7, 2.5, -1.23, -3.00, 0);
    drawTrapezoidFlip(gl, 13);
    setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 5.0, 2.0, 2.5, -1.23, -5.35, 0);
    drawTrapezoid(gl, 13);

	setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 0.6 * (BODY_WIDTH + 1.3), 1.8, 2.4, 1.8, -5.5, 0);
    drawTrapezoidFlip(gl, 12);
}

//----------------------------------------------------------------------------

function eye(gl, modelViewMatrix, modelViewMatrixLoc) {
	setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, EYE_ARM_WIDTH, EYE_ARM_HEIGHT, EYE_ARM_WIDTH, 0, 0.5 * EYE_ARM_HEIGHT, 0);
    drawCube(gl, 12);
	
	setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 1.4, 0.8, 0.8, 0, 4.4, 0);
    drawTrapezoidFlip(gl, 0);
    setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 1.4, 0.8, 0.8, 0, 3.6, 0);
    drawTrapezoid(gl, 0);
	setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 0.4, 0.4, 0.6, 0, 4.0, 0.25);
    drawCube(gl, 1);
}

//----------------------------------------------------------------------------

function resetModelViewMatrixToBody(modelViewMatrix, theta) {
    modelViewMatrix = rotate(0, 0, 1, 0);
    modelViewMatrix = mult(modelViewMatrix, translate(theta[POS_X], theta[POS_Y], 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[ROT_X], 1, 0, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[ROT_Y], 0, 1, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[ROT_Z], 0, 0, 1));
	return modelViewMatrix;
}

export function drawGary(gl, modelViewMatrixLoc, theta){
	var modelViewMatrix = resetModelViewMatrixToBody(modelViewMatrix, theta);
    body(gl, modelViewMatrix, modelViewMatrixLoc);

    modelViewMatrix = mult(modelViewMatrix, translate(2.6, -4.8, -0.7));
    modelViewMatrix = mult(modelViewMatrix, rotate(90, 0, 1, 0));
	modelViewMatrix = mult(modelViewMatrix, rotate(theta[EYE_R], 0, 0, 1));
    eye(gl, modelViewMatrix, modelViewMatrixLoc);
	
	modelViewMatrix = resetModelViewMatrixToBody(modelViewMatrix, theta);
    modelViewMatrix = mult(modelViewMatrix, translate(2.6, -4.8, 0.7));
	modelViewMatrix = mult(modelViewMatrix, rotate(90, 0, 1, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[EYE_L], 0, 0, 1));
    eye(gl, modelViewMatrix, modelViewMatrixLoc);
}