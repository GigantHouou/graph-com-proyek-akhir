"use strict";

import { exportedColors, exportedPoints, drawCube} from "./basic-shaping.js";
import { drawTrapezoid, drawTrapezoid2, drawTrapezoidFlip, drawTrapezoidFlip2} from "./basic-shaping.js";
import { drawTriangle, drawTriangleFlip, setupDraw } from "./basic-shaping.js";

export var colors = exportedColors;
export var points = exportedPoints;

export const ROT_X = 0;
export const ROT_Y = 1;
export const ROT_Z = 2;
export const POS_X = 3;
export const POS_Y = 4;
//export const HEAD = 5;
export const UPPER_ARM_R = 6;
export const LOWER_ARM_R = 7;
export const HAND_R = 8;
export const UPPER_ARM_L = 9;
export const LOWER_ARM_L = 10;
export const HAND_L = 11;
export const UPPER_LEG_R = 12;
export const LOWER_LEG_R = 13;
export const FOOT_R = 14;
export const UPPER_LEG_L = 15;
export const LOWER_LEG_L = 16;
export const FOOT_L = 17;

var BODY_WIDTH = 6.9;
var BODY_HEIGHT = 9.2;

var UPPER_ARM_WIDTH = 1.2;
var UPPER_ARM_HEIGHT = 3.2;

var LOWER_ARM_WIDTH = 1.2;
var LOWER_ARM_HEIGHT = 2;

var HAND_WIDTH = 1.2;
var HAND_HEIGHT = 0.6;

var UPPER_LEG_WIDTH = 1.7;
var UPPER_LEG_HEIGHT = 1.4;

var LOWER_LEG_WIDTH = 1.2;
var LOWER_LEG_HEIGHT = 2.4;


//----------------------------------------------------------------------------

function body(gl, modelViewMatrix, modelViewMatrixLoc) {
    setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, BODY_WIDTH, BODY_HEIGHT - 1.3, 2.05, 0, 2.2, 0);
    drawTriangleFlip(gl, 9);
    setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, BODY_WIDTH, 2.2, 2.05, 0, -2.8, 0);
    drawTrapezoid2(gl, 10);
	
    setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 1.0, 0.8, 0.5, 0.6, 3.2, 0.4);
    drawTrapezoidFlip(gl, 0);
    setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 1.0, 0.8, 0.5, 0.6, 2.4, 0.4);
	drawTrapezoid(gl, 0);
    setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 1.0, 0.8, 0.5, -0.6, 3.2, 0.4);
    drawTrapezoidFlip(gl, 0);
    setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 1.0, 0.8, 0.5, -0.6, 2.4, 0.4);
    drawTrapezoid(gl, 0);
	setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 0.4, 0.4, 0.4, 0.6, 2.6, 0.55);
    drawCube(gl, 1);
    setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, 0.4, 0.4, 0.4, -0.6, 2.6, 0.55);
    drawCube(gl, 1);
}

//----------------------------------------------------------------------------

function upperArm(gl, modelViewMatrix, modelViewMatrixLoc) {
    setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, UPPER_ARM_WIDTH, UPPER_ARM_HEIGHT, UPPER_ARM_WIDTH, 0, 0.5 * UPPER_ARM_HEIGHT, 0);
    drawTrapezoid(gl, 9);
}

//----------------------------------------------------------------------------

function lowerArm(gl, modelViewMatrix, modelViewMatrixLoc) {
    setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, LOWER_ARM_WIDTH, LOWER_ARM_HEIGHT, LOWER_ARM_WIDTH, 0, 0.5 * LOWER_ARM_HEIGHT, 0);
    drawTrapezoid(gl, 9);
}

//----------------------------------------------------------------------------

function hand(gl, modelViewMatrix, modelViewMatrixLoc) {
    setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, HAND_WIDTH, HAND_HEIGHT, 0.5, 0, 0.5 * HAND_HEIGHT, 0);
    drawTrapezoidFlip(gl, 9);
}

//----------------------------------------------------------------------------

function upperLeg(gl, modelViewMatrix, modelViewMatrixLoc) {
    setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, UPPER_LEG_WIDTH, UPPER_LEG_HEIGHT, 1, 0, 1.9 * (UPPER_LEG_HEIGHT - 0.4), 0);
    drawTrapezoidFlip(gl, 10);
}

//----------------------------------------------------------------------------

function lowerLeg(gl, modelViewMatrix, modelViewMatrixLoc) {
    setupDraw(gl, modelViewMatrix, modelViewMatrixLoc, LOWER_LEG_WIDTH, LOWER_LEG_HEIGHT, 1, 0, LOWER_LEG_HEIGHT - 0.4, 0);
    drawTriangleFlip(gl, 9);
}

//----------------------------------------------------------------------------

function resetModelViewMatrixToBody(modelViewMatrix, theta) {
    modelViewMatrix = rotate(0, 0, 1, 0);
    modelViewMatrix = mult(modelViewMatrix, translate(theta[POS_X], theta[POS_Y], 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[ROT_X], 1, 0, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[ROT_Y], 0, 1, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[ROT_Z], 0, 0, 1));
	return modelViewMatrix;
}

export function drawPatrick(gl, modelViewMatrixLoc, theta){
	var modelViewMatrix = resetModelViewMatrixToBody(modelViewMatrix, theta);
    body(gl, modelViewMatrix, modelViewMatrixLoc);

    modelViewMatrix = mult(modelViewMatrix, translate(1, 0.2, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[UPPER_ARM_R], 0, 0, 1));
    upperArm(gl, modelViewMatrix, modelViewMatrixLoc);

    modelViewMatrix = mult(modelViewMatrix, translate(0, UPPER_ARM_HEIGHT - 0.2, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[LOWER_ARM_R], 0, 0, 1));
    lowerArm(gl, modelViewMatrix, modelViewMatrixLoc);

    modelViewMatrix = mult(modelViewMatrix, translate(0, LOWER_ARM_HEIGHT - 0.2, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[HAND_R], 0, 0, 1));
    hand(gl, modelViewMatrix, modelViewMatrixLoc);
    
    modelViewMatrix = resetModelViewMatrixToBody(modelViewMatrix, theta);
    modelViewMatrix = mult(modelViewMatrix, translate(-1, 0.2, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[UPPER_ARM_L], 0, 0, 1));
    upperArm(gl, modelViewMatrix, modelViewMatrixLoc);

    modelViewMatrix = mult(modelViewMatrix, translate(0, UPPER_ARM_HEIGHT - 0.2, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[LOWER_ARM_L], 0, 0, 1));
    lowerArm(gl, modelViewMatrix, modelViewMatrixLoc);

    modelViewMatrix = mult(modelViewMatrix, translate(0, LOWER_ARM_HEIGHT - 0.2, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[HAND_L], 0, 0, 1));
    hand(gl, modelViewMatrix, modelViewMatrixLoc);
    
    modelViewMatrix = resetModelViewMatrixToBody(modelViewMatrix, theta);
    modelViewMatrix = mult(modelViewMatrix, translate(0.35 * BODY_WIDTH * 0.5, -2.6, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[UPPER_LEG_R], 0, 0, 1));
    upperLeg(gl, modelViewMatrix, modelViewMatrixLoc);

    modelViewMatrix = mult(modelViewMatrix, translate(0, UPPER_LEG_HEIGHT + 0.41, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[LOWER_LEG_R], 0, 0, 1));
    lowerLeg(gl, modelViewMatrix, modelViewMatrixLoc);

    modelViewMatrix = resetModelViewMatrixToBody(modelViewMatrix, theta);
    modelViewMatrix = mult(modelViewMatrix, translate(-0.35 * BODY_WIDTH * 0.5, -2.6, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[UPPER_LEG_L], 0, 0, 1));
    upperLeg(gl, modelViewMatrix, modelViewMatrixLoc);

    modelViewMatrix = mult(modelViewMatrix, translate(0, UPPER_LEG_HEIGHT + 0.41, 0));
    modelViewMatrix = mult(modelViewMatrix, rotate(theta[LOWER_LEG_L], 0, 0, 1));
    lowerLeg(gl, modelViewMatrix, modelViewMatrixLoc);	
}