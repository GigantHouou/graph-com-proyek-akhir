"use strict";

import { colors, points, drawSquidward } from "./js/squidward-shaping.js";

var gl, modelViewMatrixLoc;

var theta = [];
for (var i = 0; i < 18; i++) {
    theta.push(0);
}

//--------------------------------------------------

window.onload = function init() {

	// Get A WebGL context
	/** @type {HTMLCanvasElement} */
    var canvas = document.getElementById( "gl-canvas" );
	gl = canvas.getContext("webgl");
    if (!gl){ 
		alert("WebGL isn't available");
	}

    gl.viewport( 0, 0, canvas.width, canvas.height );

    gl.clearColor( 1, 1, 0.9, 1 );
    gl.enable( gl.DEPTH_TEST );

    //
    //  Load shaders and initialize attribute buffers
    //
    var program = initShaders( gl, "vertex-shader", "fragment-shader" );

    gl.useProgram( program );

    // Load shaders and use the resulting shader program

    program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );

    // Create and initialize  buffer objects

    var vBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, vBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(points), gl.STATIC_DRAW );

    var vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.vertexAttribPointer( vPosition, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );

    var cBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(colors), gl.STATIC_DRAW );

    var vColor = gl.getAttribLocation( program, "vColor" );
    gl.vertexAttribPointer( vColor, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vColor );
    
    modelViewMatrixLoc = gl.getUniformLocation(program, "modelViewMatrix");

    var projectionMatrix = ortho(-10, 10, -10, 10, -10, 10);
    gl.uniformMatrix4fv( gl.getUniformLocation(program, "projectionMatrix"),  false, flatten(projectionMatrix) );

    render();
}

//----------------------------------------------------------------------------
var currentFrame = 0;

var render = function() {

    theta = animFrames[currentFrame];
    currentFrame++;
    if (currentFrame >= animFrames.length) {
        currentFrame -= animFrames.length;
    }
	
    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT );

    drawSquidward(gl, modelViewMatrixLoc, theta);

    setTimeout(function() { requestAnimationFrame(render); }, 80);
}