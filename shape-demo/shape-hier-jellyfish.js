"use strict";

import { colors, points, drawJellyfish } from "./js/jellyfish-shaping.js";
import { ROT_X, ROT_Y, ROT_Z, POS_X, POS_Y } from "./js/jellyfish-shaping.js";
import { FRONT_UPPER_TENTACLE_R, FRONT_LOWER_TENTACLE_R, FRONT_TIP_TENTACLE_R } from "./js/jellyfish-shaping.js";
import { FRONT_UPPER_TENTACLE_L, FRONT_LOWER_TENTACLE_L, FRONT_TIP_TENTACLE_L } from "./js/jellyfish-shaping.js";
import { MIDDLE_UPPER_TENTACLE_R, MIDDLE_LOWER_TENTACLE_R, MIDDLE_TIP_TENTACLE_R }  from "./js/jellyfish-shaping.js";
import { MIDDLE_UPPER_TENTACLE_L, MIDDLE_LOWER_TENTACLE_L, MIDDLE_TIP_TENTACLE_L } from "./js/jellyfish-shaping.js";
import { BACK_UPPER_TENTACLE_R, BACK_LOWER_TENTACLE_R, BACK_TIP_TENTACLE_R } from "./js/jellyfish-shaping.js";
import { BACK_UPPER_TENTACLE_L, BACK_LOWER_TENTACLE_L, BACK_TIP_TENTACLE_L } from "./js/jellyfish-shaping.js";

var gl, modelViewMatrixLoc;

var theta = [];
for (var i = 0; i < 23; i++) {
    theta.push(0);
}
theta[FRONT_UPPER_TENTACLE_R] = 180;
theta[FRONT_UPPER_TENTACLE_L] = 180;
theta[MIDDLE_UPPER_TENTACLE_R] = 180;
theta[MIDDLE_UPPER_TENTACLE_L] = 180;
theta[BACK_UPPER_TENTACLE_R] = 180;
theta[BACK_LOWER_TENTACLE_L] = 180;

//----------------------------------------------------------------------------

window.onload = function init() {

	// Get A WebGL context
	/** @type {HTMLCanvasElement} */
    var canvas = document.getElementById( "gl-canvas" );
	gl = canvas.getContext("webgl");
    if (!gl){ 
		alert("WebGL isn't available");
	}
    gl.viewport( 0, 0, canvas.width, canvas.height );

    gl.clearColor( 1, 0.9, 1, 1 );
    gl.enable( gl.DEPTH_TEST );

    //
    //  Load shaders and initialize attribute buffers
    //
    var program = initShaders( gl, "vertex-shader", "fragment-shader" );

    gl.useProgram( program );

    // Load shaders and use the resulting shader program

    program = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( program );

    // Create and initialize  buffer objects

    var vBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, vBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(points), gl.STATIC_DRAW );

    var vPosition = gl.getAttribLocation( program, "vPosition" );
    gl.vertexAttribPointer( vPosition, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vPosition );

    var cBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, cBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, flatten(colors), gl.STATIC_DRAW );

    var vColor = gl.getAttribLocation( program, "vColor" );
    gl.vertexAttribPointer( vColor, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vColor );
    
	document.getElementById("x-angle").oninput = function(event) {
        theta[ROT_X] = event.target.value;
    };
    document.getElementById("y-angle").oninput = function(event) {
        theta[ROT_Y] = event.target.value;
    };
    document.getElementById("z-angle").oninput = function(event) {
        theta[ROT_Z] = event.target.value;
    };
    document.getElementById("x-pos").oninput = function(event) {
        theta[POS_X] = event.target.value;
    };
    document.getElementById("y-pos").oninput = function(event) {
        theta[POS_Y] = event.target.value;
    };

/***/    
	document.getElementById("front-upper-tentacle-r").oninput = function(event) {
        theta[FRONT_UPPER_TENTACLE_R] = event.target.value;
    };
	document.getElementById("front-lower-tentacle-r").oninput = function(event) {
        theta[FRONT_LOWER_TENTACLE_R] = event.target.value;
    };
	document.getElementById("front-tip-tentacle-r").oninput = function(event) {
        theta[FRONT_TIP_TENTACLE_R] = event.target.value;
    };
	document.getElementById("front-upper-tentacle-l").oninput = function(event) {
        theta[FRONT_UPPER_TENTACLE_L] = event.target.value;
    };
	document.getElementById("front-lower-tentacle-l").oninput = function(event) {
        theta[FRONT_LOWER_TENTACLE_L] = event.target.value;
    };
	document.getElementById("front-tip-tentacle-l").oninput = function(event) {
        theta[FRONT_TIP_TENTACLE_L] = event.target.value;
    };
	
/***/    
	document.getElementById("middle-upper-tentacle-r").oninput = function(event) {
        theta[MIDDLE_UPPER_TENTACLE_R] = event.target.value;
    };
	document.getElementById("middle-lower-tentacle-r").oninput = function(event) {
        theta[MIDDLE_LOWER_TENTACLE_R] = event.target.value;
    };
	document.getElementById("middle-tip-tentacle-r").oninput = function(event) {
        theta[MIDDLE_TIP_TENTACLE_R] = event.target.value;
    };
	document.getElementById("middle-upper-tentacle-l").oninput = function(event) {
        theta[MIDDLE_UPPER_TENTACLE_L] = event.target.value;
    };
	document.getElementById("middle-lower-tentacle-l").oninput = function(event) {
        theta[MIDDLE_LOWER_TENTACLE_L] = event.target.value;
    };
	document.getElementById("middle-tip-tentacle-l").oninput = function(event) {
        theta[MIDDLE_TIP_TENTACLE_L] = event.target.value;
    };
	
/***/    
	document.getElementById("back-upper-tentacle-r").oninput = function(event) {
        theta[BACK_UPPER_TENTACLE_R] = event.target.value;
    };
	document.getElementById("back-lower-tentacle-r").oninput = function(event) {
        theta[BACK_LOWER_TENTACLE_R] = event.target.value;
    };
	document.getElementById("back-tip-tentacle-r").oninput = function(event) {
        theta[BACK_TIP_TENTACLE_R] = event.target.value;
    };
	document.getElementById("back-upper-tentacle-l").oninput = function(event) {
        theta[BACK_UPPER_TENTACLE_L] = event.target.value;
    };
	document.getElementById("back-lower-tentacle-l").oninput = function(event) {
        theta[BACK_LOWER_TENTACLE_L] = event.target.value;
    };
	document.getElementById("back-tip-tentacle-l").oninput = function(event) {
        theta[BACK_TIP_TENTACLE_L] = event.target.value;
    };
	
	document.getElementById("print-slider-values").onclick = function() {
        console.log(theta.toString());
    };
	
    modelViewMatrixLoc = gl.getUniformLocation(program, "modelViewMatrix");

    var projectionMatrix = ortho(-10, 10, -10, 10, -10, 10);
    gl.uniformMatrix4fv( gl.getUniformLocation(program, "projectionMatrix"),  false, flatten(projectionMatrix) );

    render();
}

//-----------------------------------------------------------------------

var render = function() {

    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT );

    drawJellyfish(gl, modelViewMatrixLoc, theta);
	
	requestAnimationFrame(render);
}