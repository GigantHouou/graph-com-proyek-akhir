"use strict";

window.onload = function init(){
	
	// Get A WebGL context
	/** @type {HTMLCanvasElement} */
    var canvas = document.getElementById( "gl-canvas" );
	var gl = canvas.getContext("webgl");
    if (!gl){ 
		alert("WebGL isn't available");
	}

    gl.viewport( 0, 0, canvas.width, canvas.height );
    gl.clearColor( 1.0, 1.0, 1.0, 1.0 );

    gl.enable(gl.DEPTH_TEST);

	main(gl)	
}

function main(gl){
	
    //  Setup GLSL program by loading shaders and initializing attribute buffers
    var program = webglUtils.createProgramFromScripts(gl, ["vertex-shader-3d", "fragment-shader-3d"]);

	// look up where the vertex data needs to go.
	var positionLocation = gl.getAttribLocation(program, "a_position");
	var texcoordLocation = gl.getAttribLocation(program, "a_texcoord");

	// lookup uniforms
	var matrixLocation = gl.getUniformLocation(program, "u_matrix");
	var textureLocation = gl.getUniformLocation(program, "u_texture");

	// Create a buffer for positions
	var positionBuffer = gl.createBuffer();
	
	// Bind it to ARRAY_BUFFER (think of it as ARRAY_BUFFER = positionBuffer)
	gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
  
	// Put the positions in the buffer
	setGeometry(gl);

	// Create and bind texture.
	var texture = gl.createTexture();
	gl.bindTexture(gl.TEXTURE_2D, texture);
  
	// Fill the texture with a 1x1 blue pixel.
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE, new Uint8Array([0, 0, 255, 255]));
  
	// Asynchronously load an image with promise
	// Source: https://stackoverflow.com/questions/29516390/how-to-access-the-value-of-a-promise
	//         https://stackoverflow.com/a/52060802
	let promise = new Promise(function(resolve) {
		
		var image = new Image();
		image.src = "./../textures/IMGBIN_uv-mapping-cube-texture-mapping-three-dimensional-space-dice-png_HUiKTBqB.png";
		
		image.addEventListener('load', function() {
			// Now that the image has loaded make copy it to the texture.
			gl.bindTexture(gl.TEXTURE_2D, texture);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA,gl.UNSIGNED_BYTE, image);

			// Check if the image is a power of 2 in both dimensions.
			if (isPowerOf2(image.width) && isPowerOf2(image.height)) {
			
				// Yes, it's a power of 2. Generate mips.
				gl.generateMipmap(gl.TEXTURE_2D);
    
			} else {
				// No, it's not a power of 2. Turn of mips and set wrapping to clamp to edge
				gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
				gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
				gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
			}
			// Resolve promise by returning image.
			resolve(image);
		});
	});
	
	var fieldOfViewRadians = degToRad(60);
	var modelXRotationRadians = degToRad(0);
	var modelYRotationRadians = degToRad(0);

	// Get the starting time.
	var then = 0;

	// provide texture coordinates for the rectangle.
	var texcoordBuffer = gl.createBuffer();

	// Get loaded image resolved by promise.
	promise.then(function(image){
		

		gl.bindBuffer(gl.ARRAY_BUFFER, texcoordBuffer);
		
		// Set Texcoords.
		setTexcoords(gl, image.height, image.width);
		
		requestAnimationFrame(render);
	});
	
	function render(time){
		// convert to seconds
		time *= 0.001;
		// Subtract the previous time from the current time
		var deltaTime = time - then;
		// Remember the current time for the next frame.
		then = time;

		//webglUtils.resizeCanvasToDisplaySize(gl.canvas);

		// Tell WebGL how to convert from clip space to pixels
		gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

		gl.enable(gl.CULL_FACE);
		gl.enable(gl.DEPTH_TEST);

		// Animate the rotation
		modelYRotationRadians += -0.7 * deltaTime;
		modelXRotationRadians += -0.4 * deltaTime;

		// Clear the canvas AND the depth buffer.
		gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

		// Tell it to use our program (pair of shaders)
		gl.useProgram(program);

		// Turn on the position attribute
		gl.enableVertexAttribArray(positionLocation);

		// Bind the position buffer.
		gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

		// Tell the position attribute how to get data out of positionBuffer (ARRAY_BUFFER)
		var size = 3;          // 3 components per iteration
		var type = gl.FLOAT;   // the data is 32bit floats
		var normalize = false; // don't normalize the data
		var stride = 0;        // 0 = move forward size * sizeof(type) each iteration to get the next position
		var offset = 0;        // start at the beginning of the buffer
		gl.vertexAttribPointer(positionLocation, size, type, normalize, stride, offset);

		// Turn on the texcoord attribute
		gl.enableVertexAttribArray(texcoordLocation);

		// bind the texcoord buffer.
		gl.bindBuffer(gl.ARRAY_BUFFER, texcoordBuffer);

		// Tell the texcoord attribute how to get data out of texcoordBuffer (ARRAY_BUFFER)
		var size = 2;          // 2 components per iteration
		var type = gl.FLOAT;   // the data is 32bit floats
		var normalize = false; // don't normalize the data
		var stride = 0;        // 0 = move forward size * sizeof(type) each iteration to get the next position
		var offset = 0;        // start at the beginning of the buffer
		gl.vertexAttribPointer(texcoordLocation, size, type, normalize, stride, offset);

		// Compute the projection matrix
		var aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
		var projectionMatrix = m4.perspective(fieldOfViewRadians, aspect, 1, 2000);

		var cameraPosition = [0, 0, 2];
		var up = [0, 1, 0];
		var target = [0, 0, 0];

		// Compute the camera's matrix using look at.
		var cameraMatrix = m4.lookAt(cameraPosition, target, up);

		// Make a view matrix from the camera matrix.
		var viewMatrix = m4.inverse(cameraMatrix);

		var viewProjectionMatrix = m4.multiply(projectionMatrix, viewMatrix);

		var matrix = m4.xRotate(viewProjectionMatrix, modelXRotationRadians);
		matrix = m4.yRotate(matrix, modelYRotationRadians);

		// Set the matrix.
		gl.uniformMatrix4fv(matrixLocation, false, matrix);

		// Tell the shader to use texture unit 0 for u_texture
		gl.uniform1i(textureLocation, 0);

		// Draw the geometry.
		gl.drawArrays(gl.TRIANGLES, 0, 6 * 6);

		requestAnimationFrame(render);
	}
	
	// Math function to check if a value is a power of 2.
	function isPowerOf2(value) {
		return (value & (value - 1)) === 0;
	}

	// Math function to convert radian to degree.
	function radToDeg(r) {
		return r * 180 / Math.PI;	
	}

	// Math function to convert degree to radian.
	function degToRad(d) {
		return d * Math.PI / 180;
	}
}

// Fill the buffer with the values that define a cube.
function setGeometry(gl) {
	var positions = new Float32Array(
		[
			-0.5, -0.5,  -0.5,
    		-0.5,  0.5,  -0.5,
    		 0.5, -0.5,  -0.5,
    		-0.5,  0.5,  -0.5,
    		 0.5,  0.5,  -0.5,
    		 0.5, -0.5,  -0.5,

    		-0.5, -0.5,   0.5,
    		 0.5, -0.5,   0.5,
    		-0.5,  0.5,   0.5,
    		-0.5,  0.5,   0.5,
    		 0.5, -0.5,   0.5,
    		 0.5,  0.5,   0.5,

    		-0.5,   0.5, -0.5,
    		-0.5,   0.5,  0.5,
    		 0.5,   0.5, -0.5,
    		-0.5,   0.5,  0.5,
    		 0.5,   0.5,  0.5,
    		 0.5,   0.5, -0.5,

    		-0.5,  -0.5, -0.5,
    		 0.5,  -0.5, -0.5,
    		-0.5,  -0.5,  0.5,
    		-0.5,  -0.5,  0.5,
    		 0.5,  -0.5, -0.5,
    		 0.5,  -0.5,  0.5,

    		-0.5,  -0.5, -0.5,
    		-0.5,  -0.5,  0.5,
    		-0.5,   0.5, -0.5,
    		-0.5,  -0.5,  0.5,
    		-0.5,   0.5,  0.5,
    		-0.5,   0.5, -0.5,

    		 0.5,  -0.5, -0.5,
    		 0.5,   0.5, -0.5,
    		 0.5,  -0.5,  0.5,
    		 0.5,  -0.5,  0.5,
    		 0.5,   0.5, -0.5,
     		 0.5,   0.5,  0.5,

    	]);
	gl.bufferData(gl.ARRAY_BUFFER, positions, gl.STATIC_DRAW);
}

// Fill the buffer with texture coordinates the cube.
function setTexcoords(gl, imageHeight, imageWidth) {
	
	// Private function to convert x-axis spatial coordinate to x-axis texture coordinate.
	// Formula:
	//         https://webglfundamentals.org/webgl/lessons/webgl-3d-textures.html
	// Tutorial of Javascript Private Function:
	//		   https://stackoverflow.com/a/55637
	var convertXToTextcoord = function(x){
		return x / (imageWidth - 1);
	}
	
	// Private function to convert y-axis spatial coordinate to y-axis texture coordinate.
	// Formula:
	//         https://webglfundamentals.org/webgl/lessons/webgl-3d-textures.html
	// Tutorial of Javascript Private Function:
	//		   https://stackoverflow.com/a/55637
	var convertYToTextcoord = function(y){
		return y / (imageHeight - 1);
	}
	
	gl.bufferData(
		gl.ARRAY_BUFFER,
			new Float32Array(
				[
				// Select dice's side number 1 from texture map.
				convertXToTextcoord(  1), convertYToTextcoord(513),
				convertXToTextcoord(  1), convertYToTextcoord(768),
				convertXToTextcoord(255), convertYToTextcoord(513),
				convertXToTextcoord(  1), convertYToTextcoord(768),
				convertXToTextcoord(255), convertYToTextcoord(768),
				convertXToTextcoord(255), convertYToTextcoord(513),
        
				// Select dice's side number 2 from texture map.
				convertXToTextcoord(256), convertYToTextcoord(513),
        		convertXToTextcoord(511), convertYToTextcoord(513),
				convertXToTextcoord(256), convertYToTextcoord(768),
        		convertXToTextcoord(256), convertYToTextcoord(768),
				convertXToTextcoord(511), convertYToTextcoord(513),
        		convertXToTextcoord(511), convertYToTextcoord(768),
				
				//  Select dice's side number 3 from texture map.
				convertXToTextcoord(512), convertYToTextcoord(513),
				convertXToTextcoord(512), convertYToTextcoord(768),
				convertXToTextcoord(767), convertYToTextcoord(513),
				convertXToTextcoord(512), convertYToTextcoord(768),
				convertXToTextcoord(767), convertYToTextcoord(768),
				convertXToTextcoord(767), convertYToTextcoord(513),
        
				//  Select dice's side number 4 from texture map.
				convertXToTextcoord(768), convertYToTextcoord(513),
				convertXToTextcoord(1023), convertYToTextcoord(513),
				convertXToTextcoord(768), convertYToTextcoord(768),
				convertXToTextcoord(768), convertYToTextcoord(768),
				convertXToTextcoord(1023), convertYToTextcoord(513),
				convertXToTextcoord(1023), convertYToTextcoord(768),
				
				//  Select dice's side number 5 from texture map.
				convertXToTextcoord(512), convertYToTextcoord(769),
				convertXToTextcoord(512), convertYToTextcoord(1023),
				convertXToTextcoord(767), convertYToTextcoord(769),
				convertXToTextcoord(512), convertYToTextcoord(1023),
				convertXToTextcoord(767), convertYToTextcoord(1023),
				convertXToTextcoord(767), convertYToTextcoord(769),
				
				//  Select dice's side number 6 from texture map.
				convertXToTextcoord(512), convertYToTextcoord(257),
				convertXToTextcoord(767), convertYToTextcoord(257),
				convertXToTextcoord(512), convertYToTextcoord(512),
				convertXToTextcoord(512), convertYToTextcoord(512),
				convertXToTextcoord(767), convertYToTextcoord(257),
				convertXToTextcoord(767), convertYToTextcoord(512),
      ]),
      gl.STATIC_DRAW);
}

// This is needed if the images are not on the same domain
// NOTE: The server providing the images must give CORS permissions
// in order to be able to use the image with WebGL. Most sites
// do NOT give permission.
// See: https://webglfundamentals.org/webgl/lessons/webgl-cors-permission.html
//function requestCORSIfNotSameOrigin(img, url) {
//	if ((new URL(url, window.location.href)).origin !== window.location.origin) {
//		img.crossOrigin = "";
//  }
//}